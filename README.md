# ABC_Widget:
=================================================================================================

`ABC_Widget` is a test application for various C++/OpenGL experiments.
Originally idea was to create a simple QOPenGLWidget able to load and display Alembic models.
Eventually I had to split the subsystems and start planning full-featured node-based editor, while this project still exists for some quick OpenGL tests.

Current state is:

  1. Alembic loader is done
  2. QOpenGLWidget and OpenGL classes are done but will be rewritten completely for the new system
  3. Camera and controls are done
  4. Nodes - prototyping stage
  5. Node editors - in progress

![Rapid Unplanned Disassembly of a test car](docs/test.png)



### ABC_Widget:
=================================================================================================


1. Util: random static utilities: Average, FrameCounter, listdir, normpath, Trekker, JRandom
2. classes.h, defs.h: global libraries, includes and definitions.
3. mat4: QMatrix4x4 on steroids: with Projection, MVP and the rest
4. Mesh : _name, _matrix, openmesh, centroid container
5. meshloader: Loading Mesh()'es from OBJ/ABC
6. NodeBase: base class for all scene objects

  * Camera : NodeBase
  * Light : NodeBase
  * Mesh : NodeBase

7. GLShader: encapsulated Qt OpenGL Shader with autoload features

6. GLObject:

	p = parent  
	points  
	colors  
	normals  
	uvs  
	GLShader  

	Primary OpenGL object that can draw itself  
	Can be created with GLObject::from_mesh()  
	Requires OpenGL context for creation and pointer to initialized QOpenGLWidget parent.

7. GLView : QOpenGLWidget

  * Input  
  * Camera  
  * Lights  
  * Scene  

  GLView -> key/mouse events -> Input state registration -> Camera actions.  
  Force_draw_timer triggers update every 1000.0ms / 60.0fps = 0.016s.  
  Loads MeshList Scene from objs/abcs folders.  
  Rudimentary scene mutation for tests.  
