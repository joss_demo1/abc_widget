#!/usr/bin/env bash

cwd=`readlink -f ./`
echo "cwd="$cwd
root=`readlink -f ../`
echo "root="$root
bdir=$cwd"/build"
echo "bdir="$bdir

if [ ! -d $bdir ]; then
	mkdir -p $bdir
fi

cd $bdir && cmake --log-level ERROR ../ -G "Ninja" "-DCMAKE_TOOLCHAIN_FILE=~/vcpkg/scripts/buildsystems/vcpkg.cmake" && cmake --build . --config Release