#!/usr/bin/env python3

import sys, os, shutil
from pathlib import Path

_mayaver = '2023'


def go(clear=False, pause=False):
	''' Build folders one by one '''
	__cwd = Path(__file__).parent
	_sep = '#'*50
	_gen = 'Visual Studio 17 2023' if sys.platform == 'win32' else 'Ninja'
	#_gen = 'Ninja'
	#_vcpkg0 = 'C:/Programs/Vcpkg/scripts/buildsystems/vcpkg.cmake' if sys.platform == 'win32' else '~/vcpkg/scripts/buildsystems/vcpkg.cmake'
	_vcpkg = Path('~/vcpkg/scripts/buildsystems/vcpkg.cmake').expanduser().absolute()
	print(f'Using "{_vcpkg}" location')
	print(f'Using "{_gen}" cmake generator')
	_pause = ' && pause' if pause else ''
	# install dir
	inst_dir = (__cwd/'_install').absolute()
	if not inst_dir.exists():
			print(f'Create installation dir: {inst_dir}')
			os.makedirs(inst_dir)
	print(f'Using installation directory: {inst_dir}')

	# build loop
	libs = ['libsimplexnoise', 'gltext', 'libjfw', 'testbed']

	for i, lib in enumerate(libs):
		print('\n'*10)
		# paths
		lib_path = (__cwd/lib).absolute()
		lib_bld = __cwd/f'_build/{lib}'

		# cleanup
		if clear and lib_bld.exists():
			print(f'Cleanup: {lib_bld}')
			shutil.rmtree(lib_bld)
		if not lib_bld.exists():
			print(f'Create: {lib_bld}')
			os.makedirs(lib_bld)
		# build
		cmd = f'cd {lib_bld} && cmake {lib_path} -G "{_gen}" "-DGEN_APP=OFF" "-DCMAKE_INSTALL_PREFIX={inst_dir}" "-DCMAKE_TOOLCHAIN_FILE={_vcpkg}" "-DMAYA_VERSION={_mayaver}" && cmake --build . --config Release && cmake -P cmake_install.cmake{_pause}'
		print(f'{_sep}\n>> Building target {i+1} of {len(libs)}: "{lib}":\n\n{cmd}.\n\n{_sep}')
		e = os.system(cmd)
		# err
		if e:
			print(f'{_sep}\n"{lib}" cmd returned {e}, stop.\n{_sep}')
			break
		pass
	pass




if __name__ == '__main__':
	go(False)
