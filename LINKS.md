Testbed_qt6:
=================================================================================================


1. Fixed QVector<> specialization
2. Fixed build type in CMakeLists.txt
3. Fixed copying of shaders/objs/abcs
4. Verbosity turned on
5. Sources split by functionality
---------------


1. Util:
	random static utilities: Average, FrameCounter, listdir, normpath, Trekker, JRandom

2. classes.h, defs.h:
	global libraries, includes and definitions.

3. mat4:
	QMatrix4x4 on steroids: with Projection, MVP and the rest

5. Mesh :
	_name, _matrix, openmesh, centroid container
6. meshloader:
	Loading Mesh()'es from OBJ/ABC

4. NodeBase: base class for all scene objects
	Camera : NodeBase
	Light : NodeBase
	Mesh : NodeBase

5. GLShader:
	Encapsulated Qt OpenGL Shader with autoload features

6. GLObject:
	p = parent
	points
	colors
	normals
	uvs
	GLShader

	Primary OpenGL object that can draw itself
	Can be created with GLObject::from_mesh()
	Requires OpenGL context for creation and pointer to initialized QOpenGLWidget parent.

7. GLView : QOpenGLWidget
	Input
	Camera
	Lights
	Scene

	GLView -> key/mouse events -> Input state registration -> Camera actions.
	Force_draw_timer triggers update every 1000.0ms / 60.0fps = 0.016s.
	Loads MeshList Scene from objs/abcs folders.
	Rudimentary scene mutation.


=================================================================================================
generalized widget to display stuff
	1. canvas to draw: QScene
	2. mouse camera navigation - translation _matrix
	3. nodes
		layout managers
		execution models
	4. graphs

https://www.expeducation.ru/ru/article/view?id=4325

------------
nodes:
3d node view widget with matrices, pan / 3d controls

base node
	add input
	add output
	compute()



1. sceneView with scrollbars=off and transformation _matrix.
A real 3d-view that is orthographic by default, but can rotate like in blender and display nodetree as a 3d-tree

2. Controller that can zoom/pan/rotate the view

3. basic primitive Node() object with:
	a. inputs / outputs
	b. compute, pre/postcompute funcs

=================================================================================================












https://www.khronos.org/opengl/wiki/Common_Mistakes
https://simonryu.tistory.com/entry/Wireframe-rendering-with-hidden-line-removal

// multilight
https://en.wikibooks.org/wiki/GLSL_Programming/GLUT/Multiple_Lights
https://stackoverflow.com/questions/15750188/packing-data-for-multi-light-fragment-shader-in-opengl-4x

// PBR
https://learnopengl.com/PBR/Theory
https://github.com/google/filament
https://google.github.io/filament/Filament.html
https://github.com/brkho/real-time-pbr










=================================================================================================
==============
LINKS:
==============
=================================================================================================

https://doc.qt.io/qtforpython-5/PySide2/QtWidgets/QOpenGLWidget.html
https://stackoverflow.com/questions/52817407/qt-qopenglwidget-how-to-modify-individual-vertices-values-in-vbo-without-using
* https://stackoverflow.com/questions/42667135/qopenglwidget-just-works-correctly-in-the-first-paint
* https://stackoverflow.com/questions/42667135/qopenglwidget-just-works-correctly-in-the-first-paint
https://stackoverflow.com/questions/30090823/qopenglwidget-wont-draw-triangle
https://stackoverflow.com/questions/34072844/qt-opengl-vbo-data-corrupted
https://www.khronos.org/opengl/wiki/Tutorial2:_VAOs,_VBOs,_Vertex_and_Fragment_Shaders_(C_/_SDL)
https://stackoverflow.com/questions/59892088/how-does-a-vbo-get-attached-to-a-vao
https://forum.qt.io/topic/91248/accelerate-qgraphicsview-with-opengl-widget-correct-up-to-date-workflow/10
https://www.programmersought.com/article/84745193921/
https://learnopengl.com/Getting-started/OpenGL
https://stackoverflow.com/questions/63033237/how-to-draw-openmesh-using-opengl
https://stackoverflow.com/questions/22023591/why-does-qmatrix4x4lookat-result-in-a-upside-down-camera
https://subscription.packtpub.com/book/game_development/9781789535303/16/ch16lvl1sec86/the-camera-class

===================================

https://answers.unity.com/questions/1359718/what-do-the-values-in-the-matrix4x4-for-cameraproj.html
https://computergraphics.stackexchange.com/questions/4637/can-one-vao-store-multiple-calls-to-glvertexattribpointer
https://cpp.hotexamples.com/examples/-/Mat4/-/cpp-Mat4-class-examples.html
https://cpp.hotexamples.com/examples/-/QMatrix4x4/-/cpp-qmatrix4x4-class-examples.html
https://doc.qt.io/archives/qt-5.5/qopenglshaderprogram.html
https://doc.qt.io/archives/qt-5.6/qtopengl-cube-geometryengine-cpp.html
https://github.com/OpenGP/htrack/blob/master/util/eigen_opengl_helpers.h
https://github.com/openwebos/qt/blob/master/examples/opengl/cube/geometryengine.cpp
https://github.com/QtOpenGL/qgl_tutorials
https://github.com/radekp/qt/blob/master/examples/opengl/shared/qtlogo.cpp
https://gitlab.inria.fr/ocarre/sofaqtquick/blob/2986447914bdb9157afc6a52cc7ecf75c18f39d5/applications-dev/plugins/SofaQtQuickGUI/Camera.cpp !!!
https://math.stackexchange.com/questions/237369/given-this-transformation-_matrix-how-do-i-decompose-it-into-translation-rotati/3554913
https://qt.developpez.com/doc/4.7/opengl-hellogl/
https://spointeau.blogspot.com/2013/12/hello-i-am-looking-at-opengl-3.html
https://stackoverflow.com/questions/14303126/use-qmatrix4x4-with-opengl-functions
https://stackoverflow.com/questions/20634627/usage-of-qopenglvertexarrayobject
https://stackoverflow.com/questions/23250160/filling-a-vbo-with-qvectorqvector3d
https://stackoverflow.com/questions/23719498/qopenglvertexarrayobject-causes-segfault-with-multiple-vbos
https://stackoverflow.com/questions/26552642/when-is-what-bound-to-a-vao
https://stackoverflow.com/questions/28917545/qmatrix4x4-model-view-projection-opengl-cant-get-scene-to-render/41166838
https://stackoverflow.com/questions/37999609/combining-vertex-array-object-with-vertex-buffer-index-buffer
https://stackoverflow.com/questions/6602036/qvector-vs-qlist
https://stackoverflow.com/questions/67801565/what-is-the-issue-with-this-qopenglwidget
https://www.kdab.com/opengl-in-qt-5-1-part-2/
https://www.qtcentre.org/threads/62267-QGLWidget-QOpenGLVertexArrayObject-no-output
https://www.qtcentre.org/threads/66481-Qt-OpenGL-(QOpenGLWidget)-Simple-Triangle
https://www.scratchapixel.com/lessons/3d-basic-rendering/perspective-and-orthographic-projection-_matrix
https://www.trentreed.net/blog/qt5-opengl-part-1-basic-rendering/
https://stackoverflow.com/questions/17717600/confusion-between-c-and-opengl-_matrix-order-row-major-vs-column-major

Qt with OpenGL:
	https://www.trentreed.net/blog/qt5-opengl-part-3b-camera-control/
	http://www.songho.ca/opengl/gl_transform.html
time monitor:
	https://www.kdab.com/opengl-in-qt-5-1-part-3/
camera in Qt:
	https://gitlab.inria.fr/ocarre/sofaqtquick/blob/2986447914bdb9157afc6a52cc7ecf75c18f39d5/applications-dev/plugins/SofaQtQuickGUI/Camera.cpp
matrices
	https://jsantell.com/model-view-projection/
Cam matrices
	https://stackoverflow.com/questions/54491410/qmatrix4x4-translate-does-not-take-any-effect
	!!! https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/lookat-function
Spaces:
	https://learnopengl.com/Getting-started/Coordinate-Systems

https://stackoverflow.com/questions/30046006/using-qopenglwidget-as-viewport-in-qabstractscrollarea
https://stackoverflow.com/questions/31323749/easiest-way-for-offscreen-rendering-with-qopenglwidget

// hidden lines:
	https://www.opengl.org/archives/resources/code/samples/advanced/advanced97/notes/node202.html
// PBR
 https://github.com/Nadrin/PBR
// ABC
https://docs.alembic.io/python/examples.html
https://github.com/alembic/cask
https://code.google.com/archive/p/alembic/wikis/CookingWithAlembic.wiki
https://groups.google.com/g/alembic-discussion/c/IRXG3upubHU?pli=1
// USD
https://code.blender.org/2019/07/first-steps-with-universal-scene-description/

// GLSL:
https://learnopengl.com/Lighting/Basic-Lighting
http://www.lighthouse3d.com/tutorials/glsl-tutorial/uniform-blocks/
https://stackoverflow.com/questions/4200224/random-noise-functions-for-glsl

!!!!! https://github.com/patriciogonzalezvivo/lygia

// TEXT
https://learnopengl.com/In-Practice/Text-Rendering
https://learnopengl.com/code_viewer_gh.php?code=src/7.in_practice/2.text_rendering/text_rendering.cpp

TODO:
 1. hidden line draw
	https://simonryu.tistory.com/entry/Wireframe-rendering-with-hidden-line-removal

 2. different shaders
 3. PBR / HDR env

// OSL
https://odederell3d.blog/2020/06/18/writing-a-basic-osl-_color-shader/

// Openmesh
https://vovaprivalov.medium.com/work-with-obj-meshes-using-openmesh-in-python-5871ac1237ae

https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/geometry/math-operations-on-points-and-vectors
https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/lookat-function
https://jsantell.com/model-view-projection/
https://www.berdov.com/works/_matrix/opredelitel-matrici/
===========================
 Qt 3D Viewer:

 Ui file with interface, including GLView
 File tree or browse button

Smart pointers
https://stackoverflow.com/questions/34433435/why-dont-the-official-qt-examples-and-tutorials-use-smart-pointers
https://wiki.qt.io/Smart_Pointers

https://github.com/docwhite/QtSamples/blob/master/QtOpenGLSSAO/src/Scene.cpp
https://github.com/WaaallEEE/LightDrops/blob/master/ropenglwidget.cpp

Layouts:
https://stackoverflow.com/questions/59219828/c-syntax-for-variable-length-array-to-be-sent-to-glsl-in-vulkan

===================

VAO:
	https://computergraphics.stackexchange.com/questions/5895/what-is-an-opengl-vao-in-a-nutshell

Fonts:
	https://stackoverflow.com/questions/22080881/how-to-render-text-in-modern-opengl-with-glsl

OpenGL and OpenMesh:
	https://stackoverflow.com/questions/63033237/how-to-draw-openmesh-using-opengl

Architecture:
	https://www.reddit.com/r/gamedev/comments/60a0o1/opengl_engine_core_architecture_and_efficient/


Fractals / Pathtracing
	http://blog.hvidtfeldts.net/index.php/category/mandelbulb/

TODO:
	1. Remove drawdb and make a DrawPrep() with lazy caching and force update flag, returning <CamList, LightList, MeshList>
	2. Add node attributes as a mapping (to store materials / random coeffs /  LifeGame data)
	3. Make a structure like:
		Root(Transform)
			World(Transform)
				Cam
				Light
				Mesh
			Underworld(Transform)
				Grid
				Axis
				SkyBox
				FXObject (quad in front of camera)


pyinstaller:
https://www.programmersought.com/article/45697140962/