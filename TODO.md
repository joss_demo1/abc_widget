Upgrading Testbed plan:

Sample 3D app:

1. Nodes
	a. GDP: nested dict with data attached by nodes. A 'DOM model of 3D scene'
	b. Node: Attributes, Serialization, Root Widget (or Python code for the widget)
		# 'core' of 3D scene
		Transform: AKA DAG scene object / matrix, Transform Widget
		# 'shapes':
		Light: Light GUI Widget
		Camera
		Points
			Curve
			Mesh
				Subdiv
		Volume (plug-in arch, VDB, sparse VDB, etc)
		Input nodes: animated Alembic, OBJ


2. Scene
	a. Build internal classes for scene hierarchy: Scene, DAG, Mesh, Curve, etc and 
	b. Support scene serialization with data blocks a-la Blender.
	c. Support instancing, referencing, nesting, cascading properties
	d. Time context
	e. Nodes iterator
	f. Save/Load support


4. System
	Fastest start possible
	Python API: expose all classes in an Object Model
	Plug-in architecture: C++ and Python


5. GUI:
	Menu
	Shelves
	Viewport marking menus
	Scene view:
		Nodes + Timeline
		Timeline view
		3D Timeline view (flat 3D view)
		3D OpenGL view of nodes a-la Gource
	Viewport
		a. Decouple Viewport from data processing.
		b. Use any good OpenGL library
		c. Use PBR shading
			OR
		c. Make C++ Objects for all OpenGL structures (texture, buffer, etc) with proper inheritance
	Attribute editor:
		d. Use built-in node widgets

6. Shading
	a. 'shader' is just an attribute on a DAG node / faces / points
	b. PBR shaders are mimicking my generic shader (probably adsk/blender standard shader or smth)


-------------------------------------------



https://www.reddit.com/r/opengl/comments/fh591v/how_should_i_preprocess_nodes_in_a_scene/
https://internalpointers.com/post/writing-custom-iterators-modern-cpp

1. *multithreaded loader - all meshes at once

2. process uvs and normals, Triangulate or draw polys

3. *use the same shader for everything
	https://stackoverflow.com/questions/39923583/use-different-shader-programs-in-opengl

4. Sort out Scene hierarchy, matrices and world-space drawing

5. Sort out ABC loader: right now it's loading world-space _mesh



--------------------

maybe get rid of glscene
	and attach globjects right to the nodes
		and traverse the scene and draw the objects
		sorting them into lists by shader
		and setting uniforms 1 time per shader


1. 








===========================================================
https://www.reddit.com/r/gamedev/comments/60a0o1/opengl_engine_core_architecture_and_efficient/


The major issue I see with this is the Renderer class.
The default oop approach of having a base class with a
virtual method for rendering works in the small case but breaks apart in practice.
In reality we want an OOP interface to something efficient because
OOP keeps our sanity and the efficiency makes everything shiny and fast.
So my usual approach usually looks more like this:

OpenGL Wrapper Layer:
	Buffer<class InternalType> : BaseBuffer - Wraps IBO,VBOs, UBOS, OtherOs.
	ShaderCode - Wraps a single compiled shader, bonus points for preprocessing #include<>
	Shader - Links together ShaderCodes into a usable shader object.
	ShaderParameterSet - Efficient mapping of Values to shader uniforms.

Renderer Organization Level:
	MeshPart
	Offset - Triangle Offset
	Length - Num Triangles
	Mesh : Geometry
	Buffer<> VBO, VAO, IBO
	List<MeshPart> Parts - Sub-Sections of your mesh that can have different materials.
	static Shader VertexShader - Geometry being the base class supplies the vertex part of the shader. That way materials are geometry agnostic.
	Model - Just a container for gluing together Meshs and Materials
	Mesh
	List<MaterialSlot>
	Transform and jazz.

Material
	Shader - Pixel shader part
	Misc material parameters

MaterialSlot - Glues together a material and a Geometry
	Material
	Shader LinkedShader - Pull this from sort of cache of shadercode tuples.
	ShaderParameterSet Params.

Actually Drawing Crap Layer:
	DrawList
		List<DrawCall> - Sortable list of non-polymorphic drawable stuff.
		Render() - Iterates and draws.
		Sort() - Sorts DrawCalls to minimize state changes.

DrawCall - Universal way of drawing something, buffers, offsets, shader binary, and shader parameters all packed into one thing.
	VAO
	Offset, Length
	Shader
	ShaderParameterSet

I find it easier to have multiple DrawLists for different types of rendering
as a more coarse way of sorting geometry for minimal state changes.
You can also sort differently on each one, so you can sort your
transparent stuff one way and your opaque another.

There's lots of hand waving in this and you can probably
squeeze more perf out of various buffer combining shenanigans.
But it gets most of the way there.
===========================================================
