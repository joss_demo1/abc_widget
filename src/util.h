#pragma once

#include "classes.h"

namespace Util
{
	// Unroll relative paths and check file existence.
	inline static std::string normpath(const std::string &_path)
	{
		std::string res;
		auto path = std::filesystem::path(_path);
		if (!path.is_absolute()) {
			auto cwd = std::filesystem::current_path();
			path = cwd /= path;
		}
		return path.string();
	}


	// get all files with given ext from folder
	inline static std::vector<std::string> listdir(const std::string &path, const std::string &ext)
	{
		// One mask
		std::vector<std::string> files;
		auto name = normpath(path);
		if (!std::filesystem::exists(name)) {
			std::cerr << fmt::format("Path does not exist!: '{}'\n", name);
			return files;
		}

		for (const auto &entry : std::filesystem::directory_iterator(name)) {
			if (entry.is_directory())
				continue;
			// Ugly hack with QStrings, I just need any tolower() right now
			const auto fext = QString(entry.path().extension().string().c_str()).toLower().toStdString();
			// if ext is empty - just grab everything
			if (ext.length() && fext == ext)
				files.push_back(entry.path().string());
		}
		return files;
	}

	inline static std::vector<std::string> listdir2(const std::string &path, const std::vector<std::string> &masks)
	{
		// Multiple masks: {".abc", ".obj"}
		std::vector<std::string> files;
		auto name = normpath(path);
		if (!std::filesystem::exists(name)) {
			std::cerr << fmt::format("Path does not exist!: '{}'\n", name);
			return files;
		}

		for (const auto &entry : std::filesystem::directory_iterator(name)) {
			if (entry.is_directory())
				continue;
			// Ugly hack with QStrings, I just need any tolower() right now
			const auto fext = QString(entry.path().extension().string().c_str()).toLower().toStdString();
			// if empty masks given - just grab everything
			if(masks.empty()) {
				files.push_back(entry.path().string());
				continue;
			}
			// or filter by extension if not
			for(auto &ext : masks)
			{
				if (ext.length() && fext == ext) {
					files.push_back(entry.path().string());
					break;
				}
			}
		}
		return files;
	}

	class Trekker
	{
	public:
		QElapsedTimer *tm;

		Trekker()
		{
			tm = new QElapsedTimer();
			tm->start();
		}

		inline void restart()
		{
			tm->restart();
		}

		inline Real sec()
		{
			return Real(tm->elapsed()) / 1000.0;
		}

		inline Int msec()
		{
			return tm->elapsed();
		}

		inline Int nsec()
		{
			return tm->nsecsElapsed();
		}

	};


	class Average
	{
	public:
		// Vars
		Int _max;
		std::vector<Real> _queue;

		Average() : _max(10)
		{
			set_max(_max);
		}

		Average(Int mx) : _max(mx)
		{
			set_max(mx);
		}

		[[nodiscard]] inline size_t size() const
		{
			return _queue.size();
		}

		void clear()
		{
			_queue.clear();
		}

		[[nodiscard]] inline Int get_max() const
		{
			return _max;
		}

		inline void set_max(Int mx)
		{
			_max = mx;
		}

		void add(Real v)
		{
			while (size() >= _max)
				_queue.erase(_queue.begin());
			_queue.push_back(v);
		}

		Real get() const
		{
			if (!size())
				return 0.0;
			Real sum = std::accumulate(_queue.begin(), _queue.end(), static_cast<Real>(0));
			auto res = sum / Real(size());
			return res;
		}
	};


	class FrameCounter
	{
		// Count FPS / number of frames / draw times
	public:
		Int _frame = 0;                    // _frame frame counter
		Real last_frame_time = 0.0;        // last frame time
		Average _avg;                    // average accumulator
		QElapsedTimer *_fps_timer;        // time between step() calls

		FrameCounter(int size = 60) : _frame(0), last_frame_time(0.0), _avg(Average(60)), _fps_timer(new QElapsedTimer())
		{
			_avg = Average(size);
			_fps_timer->start();
		};

		inline void step()
		{
			// Call that function to trigger measurements
			++_frame;
			last_frame_time = Real(_fps_timer->elapsed()) / 1000.0;
			_avg.add(last_frame_time);
			_fps_timer->restart();
		}

		[[nodiscard]] inline Real average() const
		{
			// Get average fps
			return _avg.get();
		}

		[[nodiscard]] inline Real average_fps() const
		{
			// Get average fps
			auto res = Real(1.0) / _avg.get();
			return res;
		}
	};

	// https://www.cplusplus.com/reference/random/uniform_real_distribution/
	class JRandom
	{
	public:
		typedef std::uniform_real_distribution<RealF> RealDistro;
		RealDistro distribution;
		std::random_device rd;
		std::mt19937 mt;

		JRandom(const RealF _min = 0.0, const RealF _max = 1.0)
		{
			setMinMax(_min, _max);
		}

		inline void setMinMax(RealF _min = 0.0, RealF _max = 1.0)
		{
			mt = std::mt19937(rd());
			distribution = RealDistro(_min, _max);
		}

		inline Real getReal()
		{
			return distribution(mt);
		}

		inline auto getVector()
		{
			return QVector3D(distribution(mt), distribution(mt), distribution(mt));
		}
	};


	static Real fit(Real v, Real oMin, Real oMax, Real nMin, Real nMax)
	{
		//division by zero protection
		if(oMin==oMax || nMin==nMax) return nMin;
		if(oMin>oMax) std::swap(oMin, oMax);
		return ((v-oMin)/(oMax-oMin)*(nMax-nMin)) + nMin;
	}

	static Real fitClamped(Real v, Real oMin, Real oMax, Real nMin, Real nMax)
	{
		//division by zero protection
		if(oMin==oMax || nMin==nMax) return nMin;
		if(oMin>oMax) std::swap(oMin, oMax);
		if(v>oMax) v = oMax;
		if(v<oMin) v = oMin;
		return ((v-oMin)/(oMax-oMin)*(nMax-nMin)) + nMin;
	}

	static Real clamp(Real v, Real mn, Real mx) {return std::max(std::min(v, mx), mn);}
	static Real degrees(Real rad) {return 180*rad/M_PI;}
	static Real radians(Real deg) {return M_PI*deg/180;}


}
