#pragma once

// Typedefs:
typedef unsigned short int		Bool;
typedef unsigned short int		Byte;
typedef std::vector<Byte>		Bytes;
typedef long int				Int;
typedef double					Real;
typedef float					RealF;
typedef std::string				String;
typedef std::vector<String>		StringList;

#define Int_MAX				LONG_MAX
#define Real_MAX			DBL_MAX

#define deg2rad(angleDegrees) ((angleDegrees) * M_PI / 180.0)
#define rad2deg(angleRadians) ((angleRadians) * 180.0 / M_PI)

#define DEFAULT_NAME "default"
#define DEFAULT_OBJ_NAME "default_obj"
#define DEFAULT_ABC_NAME "default_abc"
#define DEFAULT_SHADER "default"
#define DEFAULT_TEXTURE "owl.jpg"
//#define DEFAULT_COLOR QVector3D(0.25, 0.25, 0.25)
#define DEFAULT_COLOR QVector3D(0.5, 0.5, 0.5)
#define DEFAULT_BGCOLOR QVector3D(0.25, 0.25, 0.25)
#define DEFAULT_EXPOSURE 1.0f
#define DEFAULT_FPS 60.0

#define CATEGORY_NODEBASE "nodebase"
#define CATEGORY_TRANSFORM "transform"
#define CATEGORY_MESH "_mesh"
#define CATEGORY_LIGHT "light"
#define CATEGORY_CAMERA "camera"
#define CATEGORY_AUX "aux"


// Some OpenMesh defines
typedef OpenMesh::PolyMesh_ArrayKernelT<> PolyMesh;
typedef OpenMesh::TriMesh_ArrayKernelT<> TriMesh;
typedef QVector<PolyMesh*> PolyMeshPtrList;
typedef QVector<TriMesh*> TriMeshPtrList;
typedef QVector3D Point;
typedef QVector<Point> PointList;
typedef QVector<Real> DoubleList;
typedef QVector<RealF> FloatList;
