#pragma once

#include "classes.h"
#include "util.h"
#include "glview/n_mesh2.h"

// https://www.graphics.rwth-aachen.de/media/openmesh_static/Documentations/OpenMesh-6.1-Documentation/a00054.html
// https://github.com/svn2github/OpenMesh/blob/master/trunk/src/Python/Unittests/test_trimesh_normal_calculations.py


class MeshLoader
{
public:
	inline static MeshList load_meshes(const std::vector<std::string> &paths, bool threaded=true)
	{
		// Load a list of folders with _mesh files.
		// multithreading message
		if(threaded)
			std::cout << fmt::format("{}: using multi-threaded loader\n", __FUNCTION__);
		else
			std::cout << fmt::format("{}: using single-threaded loader\n", __FUNCTION__);
		// Collect files
		std::vector<std::string> all_files;
		for(auto &path : paths)
		{
			auto npath = Util::normpath(path);
			auto files = Util::listdir2(npath, {".abc", ".obj"});
			std::copy(std::begin(files), std::end(files), std::back_inserter(all_files));
		}

		// Load
		MeshList res;
		QVector<QFuture<MeshList>> futures;
		for (auto &name : all_files) {
			std::cout << fmt::format("{}: Launch thread for '{}'\n", __FUNCTION__, name);
			if(threaded) {
				QFuture<MeshList> future = QtConcurrent::run(load_mesh, name);
				futures.append(future);
			}
			else {
				auto meshlst = load_mesh(name);
				res.append(meshlst);
			}
		}
		// collect threading results
		if(threaded) {
			for (auto &fut : futures) {
				res.append(fut.result());
			}
		}

		return res;
	}

	// size, step, xN, yN, zN, center
	inline static MeshList cubes(Int xx=10, Int yy=10, Int zz=10, RealF sz = 1.0, RealF step = 1.0, QVector3D center=QVector3D(0,0,0))
	{
		MeshList res;
		auto hx = (RealF(xx) * step) / 2.0f;
		auto hy = (RealF(yy) * step) / 2.0f;
		auto hz = (RealF(zz) * step) / 2.0f;
		for(auto x=0; x<xx; ++x) {
			for(auto y=0; y<yy; ++y) {
				for(auto z=0; z<zz; ++z) {
					auto ox = -hx+(x*step);
					auto oy = -hy+(y*step);
					auto oz = -hz+(z*step);
					auto ofs = QVector3D(ox,oy,oz);
					auto pos = center + ofs;
					auto msh = cube(sz, pos);
					if(!msh.empty()) {
						res.append(msh[0]);
					}
				}
			}
		}
		return res;
	}

	inline static MeshList cube(RealF sz=1.0, QVector3D center=QVector3D(0,0,0))
	{
		// DEBUG version
		// Load dispatcher
		auto pmesh = new PolyMesh();
		QVector<OpenMesh::SmartVertexHandle> vh(8);
		auto hz = sz / 2.0;
		vh[0] = pmesh->add_vertex(PolyMesh::Point(-hz, -hz, +hz));
		vh[1] = pmesh->add_vertex(PolyMesh::Point(+hz, -hz, +hz));
		vh[2] = pmesh->add_vertex(PolyMesh::Point(+hz, +hz, +hz));
		vh[3] = pmesh->add_vertex(PolyMesh::Point(-hz, +hz, +hz));
		vh[4] = pmesh->add_vertex(PolyMesh::Point(-hz, -hz, -hz));
		vh[5] = pmesh->add_vertex(PolyMesh::Point(+hz, -hz, -hz));
		vh[6] = pmesh->add_vertex(PolyMesh::Point(+hz, +hz, -hz));
		vh[7] = pmesh->add_vertex(PolyMesh::Point(-hz, +hz, -hz));

		std::vector<OpenMesh::SmartVertexHandle> fh;
		fh.clear();
		fh.push_back(vh[0]);
		fh.push_back(vh[1]);
		fh.push_back(vh[2]);
		fh.push_back(vh[3]);
		pmesh->add_face(fh);

		fh.clear();
		fh.push_back(vh[7]);
		fh.push_back(vh[6]);
		fh.push_back(vh[5]);
		fh.push_back(vh[4]);
		pmesh->add_face(fh);

		fh.clear();
		fh.push_back(vh[1]);
		fh.push_back(vh[0]);
		fh.push_back(vh[4]);
		fh.push_back(vh[5]);
		pmesh->add_face(fh);

		fh.clear();
		fh.push_back(vh[2]);
		fh.push_back(vh[1]);
		fh.push_back(vh[5]);
		fh.push_back(vh[6]);
		pmesh->add_face(fh);

		fh.clear();
		fh.push_back(vh[3]);
		fh.push_back(vh[2]);
		fh.push_back(vh[6]);
		fh.push_back(vh[7]);
		pmesh->add_face(fh);

		fh.clear();
		fh.push_back(vh[0]);
		fh.push_back(vh[3]);
		fh.push_back(vh[7]);
		fh.push_back(vh[4]);
		pmesh->add_face(fh);

		//pmesh->UpdateNormals();

		MeshList res;
		auto msh = new Mesh(pmesh, "cube");
		msh->_shader = DEFAULT_SHADER;
		msh->_matrix.SetTranslation(center);
		res.append(msh);
		return res;
	}

	inline static MeshList grid(RealF size_x = 10, RealF size_z = 10, RealF cell_x = 1.0, RealF cell_z = 1.0, const Mat4 &tm = Mat4())
	{
		MeshList res;
		auto pmesh = new PolyMesh();

		//res->type = GLObject::T_LINES;
		//res->glshader = glshader;

		auto start_x = -size_x / 2.0f;
		auto end_x = -start_x;
		auto lines_x = (size_x / cell_x) + 1;

		auto start_z = -size_z / 2.0f;
		auto end_z = -start_z;
		auto lines_z = (size_z / cell_z) + 1;

		auto cur_x = start_x;
		for (auto x = 0; x < lines_x; x++) {
			auto p0 = QVector3D(cur_x, 0.0, start_z);
			auto p1 = QVector3D(cur_x, 0.0, end_z);
			pmesh->add_vertex(PolyMesh::Point(p0.x(), p0.y(), p0.z()));
			pmesh->add_vertex(PolyMesh::Point(p1.x(), p1.y(), p1.z()));
			cur_x += cell_x;
		}
		auto cur_z = start_z;
		for (auto z = 0; z < lines_z; z++) {
			auto p0 = QVector3D(start_x, 0.0, cur_z);
			auto p1 = QVector3D(end_x, 0.0, cur_z);
			pmesh->add_vertex(PolyMesh::Point(p0.x(), p0.y(), p0.z()));
			pmesh->add_vertex(PolyMesh::Point(p1.x(), p1.y(), p1.z()));
			cur_z += cell_z;
		}

		auto msh = new Mesh(pmesh, "cube");
		msh->_name = "grid";
		msh->_draw_type = GLObject::T_LINES;
		msh->_shader = DEFAULT_SHADER;
		msh->_matrix = tm;
		res.append(msh);
		return res;
	}

	inline static MeshList axis(RealF sz=1.0, const Mat4 &tm = Mat4())
	{
		MeshList res;
		auto pmesh = new PolyMesh();
		QVector<OpenMesh::SmartVertexHandle> vh(6);
		//
		auto c0 = QVector3D(0.0,0.0,0.0);
		auto cr = QVector3D(1.0,0.0,0.0);
		auto cg = QVector3D(0.0,1.0,0.0);
		auto cb = QVector3D(0.0,0.0,1.0);
		//
		vh[0] = pmesh->add_vertex(PolyMesh::Point(c0.x(), c0.y(), c0.z()));
		vh[1] = pmesh->add_vertex(PolyMesh::Point(cr.x(), cr.y(), cr.z()));
		vh[2] = pmesh->add_vertex(PolyMesh::Point(c0.x(), c0.y(), c0.z()));
		vh[3] = pmesh->add_vertex(PolyMesh::Point(cg.x(), cg.y(), cg.z()));
		vh[4] = pmesh->add_vertex(PolyMesh::Point(c0.x(), c0.y(), c0.z()));
		vh[5] = pmesh->add_vertex(PolyMesh::Point(cb.x(), cb.y(), cb.z()));
		//
		auto msh = new Mesh(pmesh, "cube");
		msh->_name = "axis";
		msh->_draw_type = GLObject::T_LINES;
		msh->_shader = DEFAULT_SHADER;
		msh->_matrix = tm;
		res.append(msh);
		return res;
	}

	inline static MeshList triangle(
			const Mat4 &tm = Mat4(),
			QVector3D p0=QVector3D(0,0,0),
			QVector3D p1=QVector3D(0,0,1),
			QVector3D p2=QVector3D(1,0,0)
		)
	{
		// DEBUG version
		// Load dispatcher
		auto pmesh = new PolyMesh();
		QVector<OpenMesh::SmartVertexHandle> vh(3);
		vh[0] = pmesh->add_vertex(PolyMesh::Point(p0.x(), p0.y(), p0.z()));
		vh[1] = pmesh->add_vertex(PolyMesh::Point(p1.x(), p1.y(), p1.z()));
		vh[2] = pmesh->add_vertex(PolyMesh::Point(p2.x(), p2.y(), p2.z()));

		std::vector<OpenMesh::SmartVertexHandle> fh;
		fh.clear();
		fh.push_back(vh[0]);
		fh.push_back(vh[1]);
		fh.push_back(vh[2]);
		pmesh->add_face(fh);

		MeshList res;
		auto msh = new Mesh(pmesh, "cube");
		msh->_shader = DEFAULT_SHADER;
		msh->_matrix = tm;
		res.append(msh);
		return res;
	}

private:
	inline static MeshList load_mesh(const std::string &path)
	{
		// Load dispatcher
		MeshList res;
		auto fpth = std::filesystem::path(path);
		auto fext = QString(fpth.extension().string().c_str()).toLower().toStdString();
		// check extensions
		if(fext == ".abc") {
			auto objs = load_abc(path);
			res.append(objs);
		}
		else if(fext == ".obj") {
			auto objs = load_obj(path);
			res.append(objs);
		}
		return res;
	}

	inline static MeshList load_obj(const std::string &filename)
	{
		MeshList res;
		auto name = Util::normpath(filename);
		if (!std::filesystem::exists(name)) {
			std::cerr << fmt::format("{}: File does not exist!!!: '{}'\n", __FUNCTION__, name);
			return res;
		}

		// Read
		// https://www.graphics.rwth-aachen.de/media/openmesh_static/Documentations/OpenMesh-6.1-Documentation/a00054.html
		std::cout << fmt::format("{}: Reading '{}'...\n", __FUNCTION__, name);
		auto trek = Util::Trekker();
		auto pmesh = new PolyMesh();
		auto obj = OpenMesh::IO::read_mesh(*pmesh, name);
		if (!obj) {
			std::cerr << fmt::format("\nRead error on '{}'\n", name);
			return res;
		}
		std::cout << fmt::format("{}: {} done in '{}'...\n", __FUNCTION__, name, trek.sec());

		// Here we have a valid _mesh
		res = MeshList();
		auto mesh = new Mesh(pmesh, name);
		res.append(mesh);
		return res;
	}

	inline static MeshList load_abc(const std::string &filename = "abcs")
	{
		MeshList res;
		auto name = Util::normpath(filename);
		auto path = std::filesystem::path(name);
		if (!std::filesystem::exists(path)) {
			std::cerr << fmt::format("{}: File does not exist!!!: '{}'\n", __FUNCTION__, name);
			return res;
		}
		// Load
		std::cout << fmt::format("{}: Reading '{}'...\n", __FUNCTION__, name);
		auto trek = Util::Trekker();
		//---
		auto mat = Alembic::Abc::M44d();
		mat.makeIdentity();
		static Alembic::AbcCoreFactory::IFactory s_factory;
		Alembic::Abc::IArchive archive = s_factory.getArchive(name);
		//---
		res = _load_archive(archive.getTop(), mat);
		std::cout << fmt::format("{}: {} done in '{}'...\n", __FUNCTION__, name, trek.sec());
		return res;
	}

	// Alembics
	inline static MeshList _load_archive(
			const Alembic::Abc::IObject &obj,
			const Alembic::Abc::M44d &current = Alembic::Abc::M44d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1) )
			{
		// Load ABC-2
		// https://github.com/martin-pr/embree_viewer/blob/master/src/alembic.cpp
		MeshList res;
		// Transform
		if (Alembic::AbcGeom::IXform::matches(obj.getHeader()))
		{
			Alembic::AbcGeom::IXform xform(obj, Alembic::Abc::kWrapExisting);
			Alembic::AbcGeom::XformSample value = xform.getSchema().getValue();
			Alembic::Abc::M44d matrix = value.getMatrix() * current;
			for (std::size_t i = 0; i < obj.getNumChildren(); ++i)
			{
				auto meshes = _load_archive(obj.getChild(i), matrix);
				res.append(meshes);
			}
		}
		// IPolyMesh
		else if (Alembic::AbcGeom::IPolyMesh::matches(obj.getHeader()))
		{
			Alembic::AbcGeom::IPolyMesh inmesh(obj, Alembic::Abc::kWrapExisting);
			Alembic::AbcGeom::IPolyMeshSchema::Sample value = inmesh.getSchema().getValue();
			Alembic::Abc::Int32ArraySamplePtr faceCounts = value.getFaceCounts();
			Alembic::Abc::Int32ArraySamplePtr faceIndices = value.getFaceIndices();
			Alembic::Abc::P3fArraySamplePtr positions = value.getPositions();

			// Get Alembic points
			auto omesh = new Mesh();
			omesh->_name = DEFAULT_ABC_NAME;
			omesh->_shader = DEFAULT_SHADER;
			for (std::size_t i = 0; i < positions->size(); ++i)
			{
				const Imath::V3f pf = (*positions)[i];
				Imath::V3f pd = Imath::V3d(pf.x, pf.y, pf.z) * current;
				auto pt = PolyMesh::Point(pd.x, pd.y, pd.z);
				omesh->_draw_mesh->add_vertex(pt);
			}

			// iterate faces and  get indices into points array
			std::size_t face_start = 0;
			auto fcounts = faceCounts->size();
			for (std::size_t i = 0; i < fcounts; ++i)
			{
				const auto faceNumPts = (*faceCounts)[i];
				std::vector<PolyMesh::VertexHandle> vhandles;
				for (auto j = 0; j < faceNumPts; ++j)
				{
					auto pointlist_ref = (*faceIndices)[face_start + j];
					auto v_handle = omesh->_draw_mesh->vertex_handle(pointlist_ref);
					vhandles.emplace_back(v_handle);
				}
				omesh->_draw_mesh->add_face(vhandles);
				face_start += faceNumPts;
			}
			res.append(omesh);
		}
		// Other type
		else
		{
			for (std::size_t i = 0; i < obj.getNumChildren(); ++i)
			{
				auto meshes = _load_archive(obj.getChild(i), current);
				res.append(meshes);
			}
		}
		return res;
	}
};
