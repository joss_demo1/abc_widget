#pragma once

#include "classes.h"
#include "glview/n_camera2.h"


class Input
{
	/*
		Input controller:
			1. key/mouseEvent
			2. call Input.process(key)
			3. Look up key in internal table and perform required action
			4. Outputs linear and angular speeds

	 	Linear and Angular speeds are increase and decrease in increments and get clamped between min/max speeds.
	 	clamp (lin_speed+lin_step, lin_speed_min, lin_speed_max)
	 	clamp (ang_speed+ang_step, ang_speed_min, ang_speed_max)
	 */

public:
	QObject *_parent;	// parent widget, to control it if needed
	// Public vars:
	constexpr static const RealF _m_rot_speed = 0.15;
	constexpr static const RealF _m_pan_speed = 0.2;
	constexpr static const RealF _m_zoom_speed = 0.1;
	constexpr static const RealF _m_wheel_speed = 0.1;
	constexpr static const RealF _k_speed = 0.2;
	// speed constants
	constexpr static const RealF _spd_normal = 1.0;
	constexpr static const RealF _spd_fast = 10.0;
	RealF _spd_mult;


	QPoint _mouse0, _mouse1;
	QPoint pos0;

	// Constructors
	explicit Input(QObject *p = nullptr) :
	_parent(nullptr),
	_spd_mult(_spd_normal)
	{
		_parent = p;
		_spd_mult = _spd_normal;
		kbd.clear();
		mice.clear();
		mouse_pos0 = QCursor::pos();
		mouse_move_timer.start();
	}


	// ### Mouse
	// process mouse clicks and moves
	inline void event(const QMouseEvent* e, Camera *cam)
	{
		if(cam == nullptr) {return;}
		// Process mousemoves along with clicked buttons
		if (e->type() == QEvent::MouseMove)
		{
			_mouse0 = _mouse1;
			_mouse1 = e->globalPos();
			auto delta = QPointF(_mouse1 - _mouse0);

			if(e->buttons() & Qt::LeftButton)
			{
				auto zoom = get_zoom(_mouse1, _mouse0) * _m_zoom_speed;
				cam->_matrix.Translate(0.0, 0.0, zoom);
			}
			if(e->buttons() & Qt::MiddleButton)
			{
				delta *= _m_pan_speed;
				cam->_matrix.Translate(-delta.x(), delta.y(), 0.0);
			}
			if(e->buttons() & Qt::RightButton)
			{
				delta *= -_m_rot_speed;
				cam->_matrix.RotateAround(delta.y(), cam->_matrix.VX(), cam->_matrix.Translation());
				cam->_matrix.RotateAround(delta.x(), cam->_matrix.WY(), cam->_matrix.Translation());
			}
		}
		// Register mousepress
		else if (e->type() == QEvent::MouseButtonPress)
		{
			mice.insert(std::make_pair(e->button(), true));
		}
		// Unregister mousepress
		else if (e->type() == QEvent::MouseButtonRelease)
		{
			mice.erase(e->button());
		}
	}


	// process mouse wheel
	inline void event(const QWheelEvent* e, Camera *cam)
	{
		if (e->type() == QEvent::Wheel) {
			auto degrees = e->angleDelta().y() / 8; // degrees
			RealF dolly = -RealF(degrees) * _m_wheel_speed;
			//_cam.Translate(0.0, 0.0, dolly);
			cam->_fov += dolly;
		}
	}

	// Calc Zoom translation from 2 points
	[[nodiscard]] inline static RealF get_zoom(const QPoint& a, const QPoint& b)
	{
		auto delta = QPointF(b-a);
		RealF zoom = RealF((-delta.x()) + delta.y()) / 2.0;
		return zoom;
	}


	// ### Keyboard
	// Register / unregister pressed keys
	inline void event(const QKeyEvent* e)
	{
		if (e->isAutoRepeat())
			return;

		/* Register keypress */
		if (e->type() == QEvent::KeyPress)
		{
			kbd.insert(std::make_pair(e->key(), true));
			//std::cout << fmt::format("{} pressed\n", e->key());
		}
		else if (e->type() == QEvent::KeyRelease)
		{
			kbd.erase(e->key());
			//std::cout << fmt::format("{} released\n", e->key());
		}
		//if(kbd.find(e->key()) != kbd.end())
	}


	// Get QVector3D() with all the currently pressed keys/vectors accumulated
	//inline void step(const QObject* p, Camera *cam)
	inline void step(Camera *cam)
	{
		if(cam == nullptr) { return; }
		if(find_key(Qt::Key_H)) { cam->_matrix.Clear(); }
		if(find_key(Qt::Key_Escape)) {
			//delete _parent;
			auto qglw_ptr = dynamic_cast<QOpenGLWidget *>(_parent);
			qglw_ptr->close();
			qApp->quit();
			return;
		}
		//if(find_key(Qt::Key_Escape)) { delete p; qApp->quit(); return;}
		// Check for Shift speed mod:
		_spd_mult = (find_key(Qt::Key_Shift) || find_key(Qt::Key_Alt)) ? _spd_fast : _spd_normal;


		QVector3D res;
		for(auto &pref_key_it : kbd_map)
		{
			if(find_key(pref_key_it.first))
			{
				res += pref_key_it.second * _spd_mult;
			}
		}
		cam->_matrix.Translate(res);
	}



protected:
	// Small helper: Find a keycode in a currently pressed keys
	inline bool find_key(int keycode)
	{
		auto it = kbd.find(keycode);
		if(it != kbd.end())
			return true;
		return false;
	}


private:
	typedef std::map<int, bool> intbool_dict;	// dict of IntKeyCode:Bool to see if button pressed
	typedef std::map<int, QVector3D> intvec_dict;	// dict of IntKeyCode:Vector to generate joint vector
	// keyboard / mouse buttons
	intbool_dict kbd;		// current keyboard state map<key:bool>
	intbool_dict mice;		// current mouse state map<button:bool>

	intvec_dict kbd_map = {{Qt::Key_A, QVector3D(-_k_speed, 0, 0)},
						   {Qt::Key_D, QVector3D(_k_speed, 0, 0)},
						   {Qt::Key_S, QVector3D(0, 0, -_k_speed)},
						   {Qt::Key_Z, QVector3D(0, 0, _k_speed)},
						   {Qt::Key_E, QVector3D(0, _k_speed, 0)},
						   {Qt::Key_C, QVector3D(0, -_k_speed, 0)},
						   {Qt::Key_Space, QVector3D(0, _k_speed, 0)},
						   };

	intvec_dict mice_map = {{Qt::LeftButton,   QVector3D(-_k_speed, 0, 0)},
							{Qt::RightButton,  QVector3D(_k_speed, 0, 0)},
							{Qt::MiddleButton, QVector3D(0, 0, _k_speed)},};

	// rotational mouse speed
	QElapsedTimer mouse_move_timer;
	QPoint mouse_pos0;

};

Q_DECLARE_TYPEINFO(Input, Q_MOVABLE_TYPE);
