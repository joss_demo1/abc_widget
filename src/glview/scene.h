#pragma once
#pragma warning(disable:4305)

#include "classes.h"
#include "util.h"
#include "glview/n_light2.h"
#include "glview/n_camera2.h"
#include "glview/n_mesh2.h"
#include "data_loader/mesh_loader.h"


/*
====================================================
 Scene
	---
	World
		light1
			_glo
		transform1
			_glo
			cam1
				_glo
		mesh1
			_glo
			mesh2
				_glo
				mesh3
					_glo
	---
	Underworld
		mesh(grid)
			_glo
		mesh(axis)
			_glo
	---

	Transform:
		_dirty			// mesh dirty
		matrix._dirty	// matrix dirty
		Add recursive worldMatrix()

====================================================


 1. Scene consists of a World and Underworld nodes:
	World is for world object
	Underworld for system draw objects.


 2. Every Transform has _globject (draw_mesh)
	MeshNode has _draw_mesh == _mesh


 3. Ls() iterates all nodes
	Filter(list) - filters list by type


 4.  typedef QMap<QString, TransformList*> ObjsDict;
	 typedef QMap<QString, Transform*> ShaderDict;
	 struct DrawStruct {
		CameraList cams;
		LightList lights;
		ObjsDict objs;
	 };
	//
	DrawStruct* draw_sort()


5. Scene.draw_struct = update()
 Need to get sorted scene repr:
	cams
	lights
	{shd : [objs], shd2 : [objs2]}

6. draw(Scene.draw_struct)
	get cur _cam from camlist, set _cam uniforms on all shaders
	get cur lights from lightlist, set light uniforms on all shaders
	iterate drawdict
		find shader in Scene.glshaders
		bind shader
		iterate objs
			obj.draw(shader)
				shader.set model matrix uniform
				shader.set material uniforms
				draw()
					set VAO
					drawbuffers



// Think on how integrate Scene and other objects like Life game

// 1. Scene.Init(): Add LifeScene
// 2. Scene.UpdateDrawData: Inject meshes into drawdb.meshes
// 3. Scene.Mutate -> LifeScene.Mutate -> Cell.Mutate



*/


// Cell
class Cell {
private:
	RealF default_hp = 1.0;

public:
	RealF hp=default_hp;
	Int state;	// -1: thin, 0: no change, 1: grow
	RealF rnd;	// per cell random coefficient, "soul"

	Cell(RealF in_hp=1.0f)
	{
		hp = in_hp;
		rnd = 0.0;
		state = 0;
	}

	void Mutate() {
		if(state == 0) return;
		if(state == 1) { hp += rnd; return; }
		if(state == -1) { hp -= rnd; return; }
	}
};

// Cell Offset structure
class Offset {
public:
	Int x, y, z;

	Offset() : x(0), y(0), z(0) {}

	Offset(Int xx, Int yy, Int zz) {
		x = xx;
		y = yy;
		z = zz;
	}
};

class LifeScene {
// 1. Scene.Init(): Add LifeScene
// 2. Scene.UpdateDrawData: Inject meshes into drawdb.meshes
// 3. Scene.Mutate -> LifeScene.Mutate -> Cell.Mutate
private:
	Util::JRandom rnd_gen;

public:
	typedef QVector<Cell*> CellList;
	typedef QVector<RealF> RealFList;
	CellList grid;	// main 3d game field
	MeshList meshes;

	Int sx = 0;
	Int sy = 0;
	Int sz = 0;
	RealF inc = 0.001;
	// Cell Kernel
	typedef QVector<Offset> OffsetList;
	OffsetList offsets = {
			Offset(-1, 0, 0),
			Offset(1, 0, 0),
			Offset(0, -1, 0),
			Offset(0, 1, 0),
			Offset(0, 0, -1),
			Offset(0, 0, 1),
	};

	LifeScene(Int x=5, Int y=5, Int z=5, RealF cell_sz=1.0) :
		grid(CellList()),
		meshes(MeshList())
	{
		Init(x, y, z, cell_sz);
	}

	LifeScene(const LifeScene &b) {
		*this = b;
	}

	~LifeScene() { Clear(); }

	void Clear()
	{
		sx = 0;
		sy = 0;
		sz = 0;
		grid.clear();
		meshes.clear();
	}

	LifeScene &operator=(const LifeScene &b) {
		if (this != &b) {
			Clear();
			sx = b.sx;
			sy = b.sy;
			sz = b.sz;
			inc = b.inc;
			grid = b.grid;
			meshes = b.meshes;
		}
		return (*this);
	}


	// Access
	[[nodiscard]] Int index(Int x, Int y, Int z) const
	{
		if(x<0 || y<0 || z<0 || x>=sx || y>=sy || z>= sz) { return -1; }
		//return (x*sx) + (y*sy) + z;
		return (z * sx * sy) + (y * sx) + x;
	}

	[[nodiscard]] Cell* cell(Int x, Int y, Int z) const
	{
		// return grid[x,y,z]
		auto idx = index(x,y,z);
		if(idx == -1) { return nullptr; }
		assert(idx < meshes.size());
		return grid[idx];
	}

	[[nodiscard]] Mesh* mesh(Int x, Int y, Int z) const
	{
		// return grid[x,y,z]
		auto idx = index(x,y,z);
		if(idx == -1) { return nullptr; }
		assert(idx < meshes.size());
		return meshes[idx];
	}

	RealF get_mass(Int x, Int y, Int z) {
		int nei_num = 0;
		RealF nei_weight = 0.0f;
		for(auto &ofs : offsets) {
			auto elem = cell(x+ofs.x, y+ofs.y, z+ofs.z);
			if(!elem) { continue; }
			nei_weight += elem->hp;
			++nei_num;
		}
		nei_weight /= RealF(nei_num);
		return nei_weight;
	}


	void Init(Int x=5, Int y=5, Int z=5, RealF cell_sz=1.0)
	{
		Clear();
		sx = x;
		sy = y;
		sz = z;
		grid = QVector<Cell*>(sx*sy*sz, nullptr);
		for(auto &g : grid) {
			g = new Cell();
			g->hp = Util::fit(rnd_gen.getReal(), 0, 1, 0.1, 1.0);
			g->rnd = Util::fit(rnd_gen.getReal(), 0, 1, 0.0001, 0.001);	// generate per-cell coefficieng
		}
		meshes = MeshLoader::cubes(sx, sy, sz, cell_sz, 1.0, QVector3D(0, 0, 0));
		for (auto &mesh: meshes) {
			mesh->UpdateNormals();
			mesh->Triangulate();
			mesh->RandomizeColor();
		}
	}

	void Mutate()
	{
		Int cntr = 0;
		for(Int x=0; x<sx; ++x) {
			for(Int y=0; y<sy; ++y) {
				for(Int z=0; z<sz; ++z) {
					auto cur_cell = cell(x,y,z);
					auto cur_mesh = mesh(x,y,z);
					RealF mass = get_mass(x, y, z);
					// Logic
					RealF mn = 0.0005;
					RealF mx = 0.005;
					if(cur_cell->state == 1 && cur_cell->hp >= 1.1) {
						cur_cell->rnd = Util::fit(rnd_gen.getReal(), 0, 1, mn, mx);
						cur_cell->state = -1;
					}
					else if (cur_cell->state == -1 && cur_cell->hp <= 0.2) {
						//cur_cell->rnd = Util::fit(rnd_gen.getReal(), 0, 1, mn, mx);
						cur_cell->state = 1;
					}
					else if (cur_cell->state == 0) {
						cur_cell->rnd = Util::fit(rnd_gen.getReal(), 0, 1, mn, mx);
						cur_cell->state = 1;
					}
					cur_cell->Mutate();
					cur_mesh->_matrix.SetScale(cur_cell->hp);
					++cntr;
				}
			}
		}
		//std::cout << fmt::format("{}: {} cubes done\n", __FUNCTION__, cntr);
	}


	// end
};








struct DrawData {
	// Small structure for presorted drawing by Scene.Draw()
	Camera *cam=nullptr;
	CameraList cameras;
	LightList lights;
	MeshList meshes;
	GLShaderDict shaders;
};


class Scene
{
public:
	// Sorted scene
	Int _frame;
	QVector3D _bgcolor;
	// Draw caches
	DrawData drawdata;
	// Scene
	Transform *world;
	// Other
	Int _draw_clear;
	FloatList _tt;    // Temp coefficients
	LifeScene _life;


	Scene(bool init=false) :
	_frame(0),
	_bgcolor(DEFAULT_BGCOLOR),
	world(nullptr),
	_tt(FloatList()),
	_draw_clear(0)
	{
		if(init) {
			Init();
		}
	}

	~Scene() {
		Clear();
	}

	inline void Clear()
	{
		auto nodes = Ls(world);
		for(auto &node : nodes) {
			delete node;
			node = nullptr;
		}
		world = new Transform();
		world->_name = "world";
	}

	inline std::string Info() const
	{
		std::stringstream ss;
		ss.precision(4);
		auto objs = Ls(world);
		for(auto &obj : objs) {
			ss << fmt::format("{}: '{}(_name: '{}', shader: '{}')'\n", __FUNCTION__, obj->Category(), obj->_name, obj->_shader);
		}
		auto shaders = Shaders();
		ss << fmt::format("{}: Shaders:\n", __FUNCTION__);
		for(auto &shd : shaders) {
			ss << fmt::format("{}:\t'{}'\n", __FUNCTION__, shd);
		}
		return ss.str();
	}

	friend std::ostream &operator<<(std::ostream &ostr, const Scene &b)
	{
		auto info = b.Info();
		ostr << info;
		return ostr;
	}

	inline void InitDefault()
	{
		// Default Camera and Lights
		std::cout << fmt::format("{}: Start\n", __FUNCTION__);
		Clear();
		// Camera
		auto c = new Camera();
		c->_matrix.Rotate(-20.0, c->_matrix.WX());
		c->_matrix.Translate(0.0, 3.0, 5.0, true);
		c->_name = "camera1";
		world->AppendChildren(dynamic_cast<Transform *>(c));

		// Lights
		auto light1 = new Light(QVector3D(5.0, 0.0, 0.0), QVector3D(0.25, 0.0, 1.0));
		light1->_name = "light1";
		light1->_exposure = 5.0;
		world->AppendChildren(dynamic_cast<Transform *>(light1));

		auto light2 = new Light(QVector3D(-5.0, 0.0, 0.0), QVector3D(1.0, 0.2, 0.0));
		light2->_name = "light2";
		light2->_exposure = 5.0;
		world->AppendChildren(dynamic_cast<Transform *>(light2));

		// Grid and Axis
		auto grid = MeshLoader::grid(20, 20);
		auto axis = MeshLoader::axis();
		world->AppendChildren(dynamic_cast<Transform *>(grid[0]));
		world->AppendChildren(dynamic_cast<Transform *>(axis[0]));
	}

	inline void Init()
	{
		std::cout << fmt::format("{}: Start\n", __FUNCTION__);
		InitDefault();

		// Scene
		auto trek = Util::Trekker();
		// Load meshes
		Int npts = 0;
		Int nmeshes = 0;
		if (1) {
			// Load meshes
			MeshList meshes;

			if(0) {
				// Create cubes
				auto cbs = MeshLoader::cubes(5, 5, 5, 1.0, 1.0, QVector3D(0, 0, 0));
				meshes.append(cbs);
			}
			if(1) {
				// Load meshes
				auto loaded = MeshLoader::load_meshes({"objs", "abcs"});
				meshes.append(loaded);
			}

			if(0) {
				// Create triangle
				auto mm = Mat4();
				mm.Translate(0, 0, 0);
				auto tri = MeshLoader::triangle(mm);
				meshes.append(tri[0]);
			}

			if(0) {
				// Tripod of cubes
				PointList pts = {
					QVector3D(1,0,0),
					QVector3D(2,0,0),
					QVector3D(3,0,0),
					QVector3D(0,0,1),
					QVector3D(0,0,2),
					QVector3D(0,0,3),
					QVector3D(0,1,0),
					QVector3D(0,2,0),
					QVector3D(0,3,0),
				};
				for (auto &p : pts) {
					auto cube = MeshLoader::cube(0.5, p);
					meshes.append(cube[0]);
				}
			}

			// TODO: LIFE: Init() creates objects and UpdateDrawData injects pointes to drdata.meshes;
			if(1) {
				_life = LifeScene(10, 10, 10, 1.0);
				for (auto &mesh: _life.meshes) { npts += mesh->size(); }
				nmeshes += _life.meshes.size();
			}

			// Process all geo
			for (auto mesh: meshes) {
				mesh->UpdateNormals();
				mesh->Triangulate();
				mesh->RandomizeColor();
				world->AppendChildren(dynamic_cast<Transform *>(mesh));
				npts += mesh->size();
			}
			nmeshes = meshes.size();
		}
		std::cout << fmt::format("{}: Loaded {} objects of {} points in {} seconds.\n", __FUNCTION__, nmeshes, npts, trek.sec());
	}


	inline void Mutate(Int frame)
	{
		if(drawdata.cam == nullptr) { return; }
		auto fframe = RealF(frame);
		auto rad_frame = Util::radians(fframe);

		// random coefficients once
		if (_tt.size() != drawdata.meshes.size()) {
			_tt.clear();
			auto gen = Util::JRandom(-1.0, 1.0);
			for (auto i = 0; i < drawdata.meshes.size(); i++)
				_tt.append(RealF(gen.getReal()));
		}

		// Constant camera movement
		drawdata.cam->_matrix.Translate(0.0, 0.0001, 0.0);
		//drawdata.cam->_matrix.Rotate(1.0, _cam._m_transform.WY(), QVector3D());


		// Lights
		if(1) {
			auto ltime = rad_frame + (100.0*1.0);
			auto sine = std::sin(ltime);
			auto val = Util::fit(sine, -1, 1, -40, 40);
			drawdata.lights[0]->_matrix.SetTranslation(val, 5.0, 0.0);
			drawdata.lights[1]->_matrix.SetTranslation(-val, -5.0, 0.0);
		}


		// Meshes
		if(1) {
			for (auto i = 0; i < drawdata.meshes.size(); i++) {
				auto &mesh = drawdata.meshes[i];
				if (mesh->_name == "axis" || mesh->_name == "grid") {continue;}
				auto &coeff = _tt[i];
				//mesh->_matrix.RotateAround(coeff * 0.01, QVector3D(0, 1, 0), QVector3D());
				//mesh->_matrix.RotateAround(coeff * 0.012, QVector3D(1, 0, 0), QVector3D());
				auto local_time = Util::radians(fframe) + (100.0 * coeff);
				auto sine = std::sin(local_time);
				auto val = Util::fit(sine, -1, 1, 0.9, 1.1);
				mesh->_matrix.Translate(0.0, coeff * 0.001, 0.0, true);
				mesh->_matrix.SetScale(val * std::abs(coeff));
			}
		}

		// TODO: LIFE
		if(1) { _life.Mutate(); }
	}

	void UpdateDrawData(QObject* p, DrawData &drdata)
	{
		// Sort DAG into various lists for drawing into DrawData structure
		if(p == nullptr) { return; }

		std::cout << fmt::format("{}: Updating draw data...\n", __FUNCTION__);
		auto trek = Util::Trekker();

		// Update camera
		std::cout << fmt::format("{}: Setting camera...\n", __FUNCTION__);
		drdata.cameras = Cameras();
		if(!drdata.cameras.empty()) {
			drdata.cam = drdata.cameras[0];
		}

		// Lights
		std::cout << fmt::format("{}: Setting lights...\n", __FUNCTION__);
		drdata.lights = Lights();

		// Get Meshes and update their GLObjects (and load shaders on demand)
		std::cout << fmt::format("{}: Setting meshes...\n", __FUNCTION__);
		drdata.meshes = Meshes();

		// TODO: INJECT MESHES HERE - LIFE
		drdata.meshes.append(_life.meshes);

		std::cout << fmt::format("{}: Updating {} mesh GLObjects...\n", __FUNCTION__, drdata.meshes.size());
		for(auto &mesh : drdata.meshes) {
			mesh->update_glo(p, drdata.shaders);
		}
		std::cout << fmt::format("{}: GLObjects done...\n", __FUNCTION__);


		std::cout << fmt::format("{}: Updating data done in '{}'...\n", __FUNCTION__, trek.sec());
		_draw_clear = 1;
	}

	void Draw(QObject* p)
	{
		// Check context
		if(!_draw_clear) { return; }
		if(p == nullptr) { return; }
		// UpdateDrawData(p, drawdata);
		// Set Uniforms on all shaders
		for(auto &glshader : drawdata.shaders) {
			glshader->program->bind();
			// Camera
			auto view = drawdata.cam->_matrix.View();
			glshader->program->setUniformValue("mview", view);
			auto proj = drawdata.cam->Projection();
			glshader->program->setUniformValue("mproj", proj);

			// Light
			if(!drawdata.lights.empty()) {
				// set number of lights
				glshader->program->setUniformValue("num_lights", GLuint(drawdata.lights.size()));
				for(auto i=0; i<drawdata.lights.size(); i++) {
					auto light = drawdata.lights[i];
					auto lname = fmt::format("lights[{}]", i);
					auto lmat = fmt::format("{}.{}", lname, "matrix");
					auto lcol = fmt::format("{}.{}", lname, "color");
					auto lexp = fmt::format("{}.{}", lname, "exposure");
					glshader->program->setUniformValue(lmat.c_str(), light->_matrix);
					glshader->program->setUniformValue(lcol.c_str(), light->_color);
					glshader->program->setUniformValue(lexp.c_str(), light->_exposure);
				}
			}
			//glshader->program->release();
		}

		// draw geo
		for (auto &mesh : drawdata.meshes) {
			mesh->draw(p);
		}
		_frame++;
	}




	[[nodiscard]] inline CameraList Cameras() const
	{
		CameraList res;
		auto trs = Ls(world, "camera");
		for (auto &obj: trs) {
			res.append(dynamic_cast<Camera *>(obj));
		}
		return res;
	}

	[[nodiscard]] inline LightList Lights() const
	{
		LightList res;
		auto trs = Ls(world, "light");
		for (auto &obj: trs) {
			res.append(dynamic_cast<Light *>(obj));
		}
		return res;
	}

	[[nodiscard]] inline MeshList Meshes() const
	{
		MeshList res;
		auto trs = Ls(world, "_mesh");
		for (auto &obj: trs) {
			res.append(dynamic_cast<Mesh *>(obj));
		}
		return res;
	}

	[[nodiscard]] inline StringList Shaders() const
	{
		// Return list of all shaders in scene (unique)
		auto trs = Ls(world);
		QSet<QString> sres;
		for (auto &obj: trs) {
			auto shd = QString::fromStdString(obj->_shader);
			sres.insert(shd);
		}
		StringList res;
		for(auto &name : sres) {
			res.push_back(name.toStdString());
		}
		return res;
	}

	[[nodiscard]] inline TransformList Ls(Transform *obj, const std::string &_type = "") const
	{
		TransformList res;
		if(obj == nullptr)
			return res;
		// AppendChildren obj
		//std::cout << fmt::format("{}: '{}('{}')'\n", __FUNCTION__, obj->Category(), obj->_name);
		if (obj->Category() == _type || _type.empty()) {
			res.append(obj);
		}
		// Iterate over the children
		for (auto &child : obj->_children) {
			res.append(Ls(child, _type));
		}
		return res;
	}


	inline static Int test()
	{
		auto scn = new Scene();
		auto c = scn->Cameras();
		for(auto &cc : c)
			std::cout << fmt::format("{}: '{}('{}')'\n", __FUNCTION__, cc->Category(), cc->_name);
		auto l = scn->Lights();
		for(auto &ll : l)
			std::cout << fmt::format("{}: '{}('{}')'\n", __FUNCTION__, ll->Category(), ll->_name);
		auto m = scn->Meshes();
		for(auto &mm : m)
			std::cout << fmt::format("{}: '{}('{}')'\n", __FUNCTION__, mm->Category(), mm->_name);
		return 0;
	}
};
