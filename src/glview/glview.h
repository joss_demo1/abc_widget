#pragma warning(disable:4305)
#include "classes.h"
#include "util.h"
#include "glview/input.h"
#include "glview/scene.h"
#include "glview/globject.h"

// https://stackoverflow.com/questions/39923583/use-different-shader-programs-in-opengl
// TODO: crash on close if gl_print used
// https://github.com/WaaallEEE/LightDrops/blob/master/ropenglwidget.cpp
// https://github.com/docwhite/QtSamples/blob/master/QtOpenGLSSAO/src/Scene.cpp


class GLView : public QOpenGLWidget, protected QOpenGLFunctions_4_5_Core
{
Q_OBJECT

private:
	// Widget
	QString _qt_company = "Joss";
	QString _qt_app = "GLView_test";
	QObject* _parent;
	// Draw
	Int _frame_delay;
	QTimer *_force_draw_timer;
	Util::FrameCounter _counter;
	std::string status_str;	// status string for draw() method

public:
	// Scene
	Input _input;
	Scene* _scene;

public:
	explicit GLView(QWidget *p = nullptr) :
	QOpenGLWidget(p),
	_frame_delay(Int(1000.0/DEFAULT_FPS)),
	_force_draw_timer(nullptr),
	_scene(new Scene(false))
	{
		// All the init is happening in InitilizeGL()
		_parent = p;
		QCoreApplication::setOrganizationName(_qt_company);
		QCoreApplication::setOrganizationDomain(_qt_company);
		QCoreApplication::setApplicationName(_qt_app);
		setAttribute(Qt::WA_DeleteOnClose);
	}

	virtual ~GLView() override {
		// getting cleared in closeEvent
		//Clear();
	}

	inline void InitUI()
	{
		//setFocusPolicy(Qt::ClickFocus);
		setFocusPolicy(Qt::StrongFocus);
		setFocus();
		setMouseTracking(true);
		loadUI();
	}

	inline void InitData()
	{
		// All init_* methods should be called from InitializeGL()!!!1
		_input = Input(this);
		_scene = new Scene(false);
		_frame_delay = 1000 / DEFAULT_FPS;    // draw timer delay, ms
		_force_draw_timer = new QTimer(this);
		_force_draw_timer->start(_frame_delay);
	}

	inline void InitSignals()
	{
		// connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(Clear()));
		// Weird that it doesn't work:
		//connect( _parent, SIGNAL(destroyed(QObject*)), this, SLOT(Clear(QObject*)));
		// Force re-draw on draw_timer
		connect(_force_draw_timer, SIGNAL(timeout()), this, SLOT(update()));
		connect(this, SIGNAL(destroyed()), this, SLOT(Clear()));
	}

	inline void SetScene(Scene *scn=nullptr) {
		_scene = scn;
		_scene->UpdateDrawData(this, _scene->drawdata);
		resizeGL(width(), height());
	}

	inline void Clear() {
		// TODO check why it's crashing on delete
		_force_draw_timer->stop();
		saveUI();
		if(_scene != nullptr) {
			// TODO: Crash on delete
			delete _scene;
			_scene = nullptr;
		}
	}


protected:
	void initializeGL()
	{
		QOpenGLWidget::initializeGL();    // does makeCurrent() as well
		initializeOpenGLFunctions();
		qDebug() << "Widget OpenGl: " << format().majorVersion() << "." << format().minorVersion();
		qDebug() << "Context valid: " << context()->isValid();
		qDebug() << "Really used OpenGl: " << context()->format().majorVersion() << "." << context()->format().minorVersion();
		qDebug() << "OpenGl information: VENDOR:       " << (const char*)glGetString(GL_VENDOR);
		qDebug() << "                    RENDERDER:    " << (const char*)glGetString(GL_RENDERER);
		qDebug() << "                    VERSION:      " << (const char*)glGetString(GL_VERSION);
		qDebug() << "                    GLSL VERSION: " << (const char*)glGetString(GL_SHADING_LANGUAGE_VERSION);

		// Main widget initialization IS HERE:
		InitUI();
		InitData();
		InitSignals();
		// Main widget initialization IS HERE:

		// Use default or scene bgcolor:
		auto bgcol = DEFAULT_BGCOLOR;
		if(_scene != nullptr) {bgcol = _scene->_bgcolor;}
		glClearColor(bgcol.x(), bgcol.y(), bgcol.z(), 1.0);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
		glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
		glEnable(GL_COLOR_MATERIAL);
		glShadeModel(GL_FLAT);
	}

	void paintGL()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		_input.step(_scene->drawdata.cam);
		// If Esc was pressed, scene will be destroyed, we need to check it here:
		if(_scene == nullptr) { return; }
		_scene->Mutate(_counter._frame);
		_scene->Draw(this);
		_counter.step();
	}

	void resizeGL(int w, int h)
	{
		if(_scene == nullptr || _scene->drawdata.cam == nullptr) {
			return;
		}
		_scene->drawdata.cam->resize(w, h);
	}


protected Q_SLOTS:
	// --= Keyboard events
	void keyPressEvent(QKeyEvent *kEvent) override
	{
		//QOpenGLWidget::keyPressEvent(kEvent);
		if (kEvent->isAutoRepeat()) {
			kEvent->ignore();
			return;
		}
		_input.event(kEvent);
		kEvent->accept();
	}

	void keyReleaseEvent(QKeyEvent *kEvent) override
	{
		_input.event(kEvent);
		kEvent->accept();
	}

	// --= Mouse events
	void mousePressEvent(QMouseEvent *mEvent) override
	{
		_input.event(mEvent, _scene->drawdata.cam);
		mEvent->accept();
	}

	void mouseReleaseEvent(QMouseEvent *mEvent) override
	{
		_input.event(mEvent, _scene->drawdata.cam);
		mEvent->accept();
	}

	void wheelEvent(QWheelEvent *mEvent) override
	{
		_input.event(mEvent, _scene->drawdata.cam);
		mEvent->accept();
	}

	void mouseMoveEvent(QMouseEvent *mEvent) override
	{
		_input.event(mEvent, _scene->drawdata.cam);
		mEvent->accept();
	}

	void closeEvent(QCloseEvent *event) override
	{
		std::cout << fmt::format("{}\n", __FUNCTION__ );
		Clear();
		event->accept();
	}

	// --== Overrides
	[[nodiscard]] QSize minimumSizeHint() const override
	{
		return {50, 50};
	}

	[[nodiscard]] QSize sizeHint() const override
	{
		//return {parentWidget()->width(), parentWidget()->height()};
		return {geometry().width(), geometry().height()};
	}

	inline void saveUI()
	{
		QSettings settings(_qt_company, _qt_app, this);
		auto fname = settings.fileName();
		auto geo = saveGeometry();
		settings.setValue("geometry", geo);
		std::cout << fmt::format("{}: saved prefs to '{}'.\n", __FUNCTION__, fname.toStdString());
	}

	inline void loadUI()
	{
		QSettings settings(_qt_company, _qt_app, this);
		auto fname = settings.fileName();
		auto qv = settings.value("geometry");
		if(qv.isNull()) {
			auto geo = QRect(QPoint(100, 100), QSize(1280, 720));
			setGeometry(geo);
			std::cout << fmt::format("{}: prefs not found, using defaults.\n", __FUNCTION__);
		} else {
			auto geo = qv.toByteArray();
			restoreGeometry(geo);
			std::cout << fmt::format("{}: loaded prefs from '{}'.\n", __FUNCTION__, fname.toStdString());
		}
	}

	/*//https://www.qtcentre.org/threads/22154-QGLWidget-keyPressEvent-does-not-work
	// events debug, add next line to constructor:
	// qApp->installEventFilter(this); OR installEventFilter(this);
	bool eventFilter(QObject *o, QEvent *e) override
	{
		if (e->type() == QEvent::KeyPress)
			qDebug() << "The bad guy which steals the keyevent is " << o;
		return false;
	}*/

public:
	void gl_print(QPainter &painter, const std::string &text, Int x = 0, Int y = 0, Int sz = 9, Real r = 0.75, Real g = 0.75, Real b = 0.75, Real a = 0.75)
	{
		auto qstr = QString::fromStdString(text);
		//glPushMatrix();
		//glPushAttrib(GL_ALL_ATTRIB_BITS);
		auto font = painter.font();
		QFontMetrics metrics = QFontMetrics(font);
		int border = qMax(2, metrics.leading());
		QRect rect = metrics.boundingRect(0, 0, width() - 2 * border, height() / 4, Qt::AlignLeft | Qt::TextWordWrap, qstr);
		QRect rectText(0, 0, rect.width() + 4 * border, rect.height() + 4 * border);
		painter.setBrush(QColor(38, 181, 242, 30));
		painter.drawRect(rectText);
		painter.drawText(
				rectText.x() + 2 * border, rectText.y(), rectText.width(), rectText.height(),
				Qt::AlignLeft | Qt::AlignVCenter | Qt::TextWordWrap, qstr);
		painter.end();
		//glPopAttrib();
		//glPopMatrix();
	}

	// Test
	inline static Int test()
	{
		char arg0[] = "test_app";
		char arg1[] = "arg0";
		char arg2[] = "arg1";
		char *argv[] = {&arg0[0], &arg1[0], &arg2[0], nullptr};
		int argc = (int) (sizeof(argv) / sizeof(argv[0])) - 1;

		//#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
		QApplication app(argc, &argv[0]);
		auto w = new QMainWindow;
		auto central_wid = new QWidget();
		//w->setWindowFlags(Qt::FramelessWindowHint);
		w->setCentralWidget(central_wid);

		auto l1 = new QHBoxLayout;
		l1->setContentsMargins(2, 2, 2, 2);

		// Format
		QSurfaceFormat format;
		format.setSamples(16);
		format.setDepthBufferSize(32);
		format.setVersion(4, 5);
		format.setProfile(QSurfaceFormat::CoreProfile);

		// QOGLW
		auto wid_gl = new GLView(w);
		wid_gl->setFormat(format);

		l1->addWidget(wid_gl);
		central_wid->setLayout(l1);

		// start app
		w->show();
		w->resize(wid_gl->geometry().width(), wid_gl->geometry().height());

		// TODO: make some default virtual function to set default empty scene and simple camera/light
		auto scn = new Scene(true);
		//std::cout << *scn << std::endl;
		wid_gl->SetScene(scn);
		//std::cout << wid_gl->_glscene->Info() << std::endl;


		auto r = app.exec();
		return r;
	}
};



/*
 PaintGL():
if(status_str.empty() || _counter._frame % 7 == 0)
{
	std::stringstream ss;
	ss.precision(4);
	ss << "Hello, Ima test!!!1\n";
	ss << "last frame: " << _counter.last_frame_time << "s" << std::endl;
	ss << "avg frame: " << _counter.average() << "s" << std::endl;
	ss << "avg fps: " << _counter.average_fps() << std::endl;
	auto ct = _cam->_matrix.Translation();
	ss << "_cam tr: " << ct.x() << ", " << ct.y() << ", " << ct.z() << std::endl;
	auto cr = _cam->_matrix.RotationEuler();
	ss << "_cam rot: " << cr.x() << ", " << cr.y() << ", " << cr.z() << std::endl;
	ss << _cam->_matrix.View() << std::endl;
	ss << _cam->MVP() << std::endl;
	status_str = ss.str();
}
if(0)
{
	QPainter pntr(this);
	QPen pen( Qt::green );
	pen.setWidth(2);
	pntr.setPen(pen);
	pntr.setRenderHints(QPainter::Antialiasing, true);
	pntr.setRenderHint(QPainter::TextAntialiasing);
	pntr.setRenderHints(QPainter::HighQualityAntialiasing, true);
	QFont font = pntr.font();
	font.setPixelSize(9);
	font.setBold(false);
	pntr.setFont(font);
}
gl_print(pntr, status_str, 20, 20, 10);
*/