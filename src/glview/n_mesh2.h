#pragma once

#include "classes.h"
#include "glview/n_transform1.h"

/*
	Basic Mesh node: _draw_mesh contains actual OpenMesh
*/

class Mesh : public Transform
{

public:

	Mesh() {
		_draw_mesh = new PolyMesh();
	}

	Mesh(PolyMesh* mesh, const std::string &name = DEFAULT_NAME)
	{
		_name = name;
		_draw_mesh = mesh;
	}

	Mesh &operator=(const Mesh &b)
	{
		if (this != &b) {
			_draw_mesh = b._draw_mesh;
		}
		return (*this);
	}

	inline std::string Category() override {
		return CATEGORY_MESH;
	}

	[[nodiscard]] Int size() const
	{
		return Int(_draw_mesh->n_vertices());
	}

	inline void Triangulate()
	{
		_draw_mesh->triangulate();
	}

	inline void UpdateNormals()
	{
		if (!_draw_mesh->has_face_normals()) { _draw_mesh->request_face_normals(); }
		if (!_draw_mesh->has_vertex_normals()) { _draw_mesh->request_vertex_normals(); }
		_draw_mesh->update_normals();
	}
};


// should use OpenMesh pointer to make "natural" instances: existing openmesh and _matrix


typedef QVector<Mesh*> MeshList;
