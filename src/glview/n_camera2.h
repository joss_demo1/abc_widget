#pragma once
#pragma warning(disable:4305)

#include "classes.h"
#include "glview/n_transform1.h"


class Camera : public Transform
{
	/* Main storage is Mat4 or Eigen3 Matrix
	 Hierarchy is:
	 Camera
	 	Transform
	 		Mat4
	*/
public:
	inline static std::string _object_type = "camera";
	// perspective _cam initial values:
	RealF _focal;
	RealF _fov;
	RealF _far;
	RealF _near;
	QRectF _window;

	Camera() :
	_focal(10.0),
	_fov(45.0),
	_near(0.01),
	_far(10000.0),
	_window(QRect(0,0,1,1))
	{

	}

	explicit Camera(
			const RealF fov_,
			const RealF near_=0.01,
			const RealF far_=10000.0,
			const QRect &window_=QRect(0,0,1,1),
			const RealF focal_ = 10.0)
	{
		_fov = fov_;
		_near = near_;
		_far = far_;
		_window = window_;
		_focal = focal_;
	}

	Camera &operator=(const Camera &b)
	{
		if (this != &b) {
			_focal = b._focal;
			_fov = b._fov;
			_far = b._far;
			_near = b._near;
			_window = b._window;
		}
		return (*this);
	}

	inline std::string Category() override {
		return CATEGORY_CAMERA;
	}

	[[nodiscard]] inline Mat4 Projection() const
	{
		auto res = Mat4::Projection(_fov, _near, _far, _window);
		return res;
	}

	[[nodiscard]] inline Mat4 MVP(const QMatrix4x4 &m_model=QMatrix4x4()) const
	{
		return Mat4::MVP(_fov, _near, _far, _window, _matrix.View(), m_model);
	}

	inline void resize(const Int w, const Int h)
	{
		_window = QRect(0,0,w,h);
	}

	friend std::ostream &operator<<(std::ostream &ostr, const Camera &b)
	{
		// OSTR_SET
		auto t = b._matrix.Translation();
		std::stringstream ss;
		ss.precision(4);
		std::stringstream smat;
		smat.precision(4);
		smat << b._matrix;
		ss << fmt::format("Camera( _matrix({}), fov={}, near={}, far={}, window={}x{} )\n", smat.str(), b._fov, b._near, b._far, b._window.width(), b._window.height());
		ostr << ss.str();
		return ostr;
	}

	inline static void test()
	{
		auto cam = Camera();
		std::cout << cam << std::endl;
	}
};


typedef QVector<Camera*> CameraList;