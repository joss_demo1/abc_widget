#pragma once
#pragma warning(disable:4305)

#include "classes.h"

// https://gitlab.inria.fr/ocarre/sofaqtquick/blob/2986447914bdb9157afc6a52cc7ecf75c18f39d5/applications-dev/plugins/SofaQtQuickGUI/Camera.cpp


class Mat4 : public QMatrix4x4
// Add decomposition and conversions to native QMatrix4x4
{
public:
	Mat4() : QMatrix4x4()
	{

	}

	Mat4(const Mat4 &b)
	{
		*this = b;
	}

	Mat4(const QMatrix4x4 &b)
	{
		for (auto i=0; i<4; i++)
		{
			this->setColumn(i, b.column(i));
		}
		// Looks like copying like this transposes the _matrix
		// b.copyDataTo(this->data());
	}

	inline void Clear() {
		setToIdentity();
	}


	friend std::ostream &operator<<(std::ostream &ostr, const Mat4 &b)
	{
		// OSTR_SET
		std::stringstream ss;
		ss.precision(4);
		ss << "Mat4(\n";
		// rows
		for (auto r = 0; r < 4; r++) {
			ss << "[";
			for (auto c = 0; c < 4; c++) {
				ss << b(c, r);
				if (r < 3) {
					ss << ", ";
				}
			}
			ss << "]" << std::endl;
		}
		ss << ")" << std::endl;
		ostr << ss.str();
		return ostr;
	}

	// local space Vectors
	[[nodiscard]] inline const static QVector3D WX() { return {1.0, 0.0, 0.0}; }
	[[nodiscard]] inline const static QVector3D WY() { return {0.0, 1.0, 0.0}; }
	[[nodiscard]] inline const static QVector3D WZ() { return {0.0, 0.0, 1.0}; }

	[[nodiscard]] QVector3D VX() const { return column(0).toVector3D().normalized(); }
	[[nodiscard]] QVector3D VY() const { return column(1).toVector3D().normalized(); }
	[[nodiscard]] QVector3D VZ() const { return column(2).toVector3D().normalized(); }

	[[nodiscard]] QVector3D Right() const { return VX(); }
	[[nodiscard]] QVector3D Up() const { return VY(); }
	[[nodiscard]] QVector3D Forward() const { return VZ(); }
	[[nodiscard]] QVector3D Direction() const { return -Forward(); }


	// Matrices
	[[nodiscard]] inline Mat4 Model() const
	{
		return *this;
	}

	[[nodiscard]] inline Mat4 View() const
	{
		// View _matrix is Camera transform inverted
		// https://stackoverflow.com/questions/22023591/why-does-qmatrix4x4lookat-result-in-a-upside-down-camera
		return Model().inverted();
	}

	[[nodiscard]] inline static Mat4 Projection(const RealF &_fov, const RealF &_near, const RealF &_far, const QRectF &_window)
	{
		QMatrix4x4 proj;
		float ratio = _window.width() / qMax(_window.height(), 1.0);
		proj.perspective(_fov, ratio, _near, _far);
		return proj;
	}

	[[nodiscard]] inline static Mat4 MVP(const RealF &_fov,
										 const RealF &_near,
										 const RealF &_far,
										 const QRectF &_window,
										 const Mat4 &m_view,
										 const Mat4 &m_model=Mat4())
										 /*
										  * _fov: vertican angle, degrees
										  * _near: near clipping plane
										  * _far: far clipping plane
										  * _window: QRect, width and height of the window
										  * m_view: inverted camera transform _matrix
										  * m_model: model _matrix
										  */
	{
		// https://cpp.hotexamples.com/examples/-/QMatrix4x4/-/cpp-qmatrix4x4-class-examples.html
		// https://stackoverflow.com/questions/28917545/Mat4-model-view-projection-opengl-cant-get-scene-to-render/41166838
		// https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/lookat-function
		// https://stackoverflow.com/questions/54491410/qmatrix4x4-translate-does-not-take-any-effect
		// https://community.khronos.org/t/how-to-design-a-building-in-opengl-c/106729
		// View Matrix is an inversion of Camera transform!!!
		// https://stackoverflow.com/questions/22023591/why-does-qmatrix4x4lookat-result-in-a-upside-down-camera
		auto proj = Projection(_fov, _near, _far, _window);
		Mat4 mvp = proj * m_view * m_model;
		return mvp;
	}











	[[nodiscard]] inline QVector3D Translation() const
	{
		// Translation
		auto res = column(3).toVector3D();
		return res;
	}


	inline void SetTranslation(const Real x, const Real y, const Real z)
	{
		// Set _matrix Translation component with QVector3D
		SetTranslation(QVector3D(x,y,z));
	}

	inline void SetTranslation(const QVector3D &v)
	{
		// Set _matrix Translation component directly
		auto col = QVector4D(v.x(), v.y(), v.z(), column(3)[3]);
		setColumn(3, col);
	}

	inline void Translate(const QVector3D &t, bool world = false)
	{
		// relative translation
		Translate(t.x(), t.y(), t.z(), world);
	}

	inline void Translate(RealF x, RealF y, RealF z, bool world = false)
	{
		// relative translation
		if(world)
		{
			// World-space translation
			auto v0 = QVector3D(WX() * x + WY() * y + WZ() * z);
			auto v1 = Translation() + v0;
			SetTranslation(v1);
		}
		else
		{
			// Hint #1: QMatrix4x4.translate() works in 5 different modes and in local space eventually. LOCAL space.
			translate(x, y, z);
		}
	}




	[[nodiscard]] inline QQuaternion Rotation() const
	{
		return QQuaternion::fromRotationMatrix(normalMatrix());
	}

	[[nodiscard]] inline QVector3D RotationEuler() const
	{
		return QQuaternion::fromRotationMatrix(normalMatrix()).toEulerAngles();
	}

	inline void Rotate(RealF ax, RealF ay, RealF az)
	{
		// "Stupid" rotation
		auto tr = Translation();
		SetTranslation(0.0,0.0,0.0);
		rotate(ay, Up());
		rotate(ax, Right());
		rotate(az, Direction());
		SetTranslation(tr);
	}

	inline void Rotate(RealF angle, QVector3D axis = QVector3D(1.0,0.0,0.0))
	{
		// TODO
		// Quat: angle/axis rotation
		auto tr = Translation();
		SetTranslation(0.0,0.0,0.0);
		QQuaternion qq = QQuaternion::fromAxisAndAngle(axis, angle);
		rotate(qq);
		SetTranslation(tr);
	}

	inline void RotateAround(RealF angle, QVector3D axis = QVector3D(1.0,0.0,0.0), QVector3D center = QVector3D(0.0,0.0,0.0))
	{
		QMatrix4x4 mat;
		mat.translate(center);
		mat.rotate(angle, axis);
		mat.translate(-center);
		*this = mat * *this;
	}



	[[nodiscard]] inline QVector3D Scaling() const
	{
		auto sx = QVector3D(column(0)).length();
		auto sy = QVector3D(column(1)).length();
		auto sz = QVector3D(column(2)).length();
		auto res = QVector3D(sx, sy, sz);
		return res;
	}

	inline void SetScale(RealF scl)
	{
		SetScale(QVector3D(scl, scl, scl));
	}

	inline void SetScale(const QVector3D &v)
	{
		auto scl = Scaling();
		//auto scl2 = scl + v;
		auto scl2 = v;
		auto vx = QVector4D(QVector3D(column(0)).normalized() * scl2.x());
		auto vy = QVector4D(QVector3D(column(1)).normalized() * scl2.y());
		auto vz = QVector4D(QVector3D(column(2)).normalized() * scl2.z());
		setColumn(0, vx);
		setColumn(1, vy);
		setColumn(2, vz);
	}


#ifdef USE_EIGEN
	// Conversions
	[[nodiscard]] static inline Eigen::Matrix4f toEigen(const QMatrix4x4 &m)
	{
		Eigen::Matrix4f res(static_cast<const float *>( m.constData()));
		return res;
	}

	[[nodiscard]] static inline QMatrix4x4 fromEigen(const Eigen::Matrix4f &m)
	{
		QMatrix4x4 res(static_cast<const float *>(m.data()));
		return res;
	}

	inline void decomposeEigen(QVector3D &scaling, QQuaternion &rotation, QVector3D &position) const
	{
		// https://stackoverflow.com/questions/34177111/how-to-convert-eigenmatrix4f-to-eigenaffine3f
		const Eigen::Matrix4f _this = toEigen(*this);
		Eigen::Affine3f _affine;
		_affine = _this;
		auto tr = _affine.translation();
		auto rot = _affine.rotation();
	}
#endif






	inline static void test()
	{
		auto mm = Mat4();
		std::cout << mm << std::endl;
	}

};
