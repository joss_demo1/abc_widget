#pragma once
#pragma warning(disable:4305)

#include "classes.h"
#include "util.h"
#include "glview/mat4.h"
#include "glview/n_nodebase0.h"
#include "glview/globject.h"


class Transform : public NodeBase
{
	// Transform DAG node
	// Basic DAG node setup: _matrix, _color, parent, children
	// Shaders can be attached just to Transform and inherited.
public:
	Mat4 _matrix;	// local _matrix
	std::string _shader;    // shader _name
	QVector3D _pivot;    // center point in local space
	Transform *_parent;
	QVector<Transform *> _children;
	Int _draw_type;	// draw type for OpenGL
	PolyMesh* _draw_mesh;	// Each Transform and derived classes have "draw_mesh": their scene representation
	GLObject* _glo;			// OpenGL draw object

	Transform() :
	_matrix(Mat4()),
	_parent(nullptr),
	_children(QVector<Transform *>()),
	_pivot(QVector3D()),
	_shader(DEFAULT_SHADER),
	_draw_type(GLObject::T_TRIS),
	_draw_mesh(nullptr),
	_glo(nullptr)
	{
	}

	~Transform()
	{
		delete _glo;
		_glo = nullptr;
	}

	inline std::string Category() override
	{
		return CATEGORY_TRANSFORM;
	}

	Transform &operator=(const Transform &b)
	{
		if (this != &b) {
			_name = b._name;
			_color = b._color;
			_matrix = b._matrix;
			_parent = b._parent;
			_children = b._children;
			// TODO: Assigning pointers, check later for copying data
			_draw_type = b._draw_type;
			_draw_mesh = b._draw_mesh;
			_glo = b._glo;
		}
		return (*this);
	}

	friend std::ostream &operator<<(std::ostream &ostr, const Transform &b)
	{
		// OSTR_SET
		auto t = b._matrix.Translation();
		std::stringstream ss;
		ss.precision(4);
		std::stringstream smat;
		smat.precision(4);
		smat << b._matrix;
		ss << fmt::format("Transform( '{}', _matrix({}), )\n", b._name, smat.str());
		ostr << ss.str();
		return ostr;
	}


	// DAG
	inline void Clear()
	{
		_glo = nullptr;
		_draw_mesh = nullptr;
		_draw_type = GLObject::T_TRIS;
		ClearMatrix();
		ClearParent();
		ClearChildren();
	}

	inline void ClearMatrix()
	{
		_matrix.Clear();
	}

	inline void ClearParent()
	{
		_parent = nullptr;
	}

	inline void ClearChildren()
	{
		_children.clear();
	}

	[[nodiscard]] inline size_t NumChildren() const
	{
		return _children.size();
	}

	inline void AppendChildren(Transform *node)
	{
		// AppendChildren a childen node
		_children.append(node);
	}

	inline void RemoveChildren(Int num)
	{
		assert(num < NumChildren());
		_children.erase(_children.begin() + num);
	}

	// Operators:
	inline Transform *operator[](Int num)
	{
		assert(num < NumChildren());
		return _children[num];
	}

	inline const Transform *operator[](Int num) const
	{
		assert(num < NumChildren());
		return _children[num];
	}

	QVector<Transform *>::iterator begin()
	{
		return _children.begin();
	}

	QVector<Transform *>::iterator end()
	{
		return _children.end();
	}

	[[nodiscard]] QVector<Transform *>::const_iterator begin() const
	{
		return _children.begin();
	}

	[[nodiscard]] QVector<Transform *>::const_iterator end() const
	{
		return _children.end();
	}

	// Transform properties
	[[nodiscard]] inline QVector3D PivotW() const
	{
		// Multiply local pivot by _matrix
		return _matrix * _pivot;
	}


	// OpenGL
	inline static GLShader* get_shader(QObject* p, const std::string &name, GLShaderDict &glshaders)
	{
		// Probably not the best place for this function...
		// Get shader from dict or create a new one if needed (and add it to dict)
		GLShader* res = nullptr;
		auto it = glshaders.find(QString::fromStdString(name));
		if( it != glshaders.end()) {
			res = it.value();
		}
		else {
			res = new GLShader(p, name);
			glshaders.insert(QString::fromStdString(name), res);
		}
		return res;
	}


	inline void update_glo(QObject* p, GLShaderDict &glshaders)
	{
		// TODO: add material data here

		//std::cout << fmt::format("{}: Getting shader:\n", __FUNCTION__);
		//auto trek = Util::Trekker();
		auto shader = get_shader(p, _shader, glshaders);
		//std::cout << fmt::format("{}: Got shader: {:.4f}...\n", __FUNCTION__, trek.sec());

		//std::cout << fmt::format("{}: Getting GLO:\n", __FUNCTION__);
		//trek.restart();
		_glo = GLObject::from_mesh(p, _draw_mesh, _draw_type, shader);
		if(!_glo) { return; }
		//std::cout << fmt::format("{}: Got GLO: {:.4f}...\n", __FUNCTION__, trek.sec());

		// Upload GPU data
		if (_glo->_vao == nullptr || _glo->_p_vbo == nullptr) {
			//std::cout << fmt::format("{}: Updating GLO:\n", __FUNCTION__);
			//trek.restart();
			_glo->update(p);
			//std::cout << fmt::format("{}: Update GLO VAO/VBO: {:.4f}...\n", __FUNCTION__, trek.sec());
		}
	}


	// static func that knows how to draw this object's _glo
	inline void draw(QObject* p)
	{
		// Camera and Light uniforms should be set on all shaders already in Scene.draw()
		// Cannot draw without a context
		if(p == nullptr) { return; }
		// Update GLO if it's nullptr
		if (_glo == nullptr || !_glo->size() || _glo->glshader == nullptr || !_glo->glshader->program->isLinked()) {
			// not autoupdating upon bad glo/shader: it should be updated from outside by Scene.UpdateDrawData()
			return;
		}
		// Draw
		_glo->glshader->program->bind();
		// Camera
		_glo->glshader->program->setUniformValue("mmodel", _matrix);
		// Material uniforms
		_glo->glshader->program->setUniformValue("material.ambient", QVector3D(0,0,0));
		_glo->glshader->program->setUniformValue("material.diffuse", _color);
		_glo->glshader->program->setUniformValue("material.specular", QVector3D(1.0,1.0,1.0));
		_glo->glshader->program->setUniformValue("material.emission", QVector3D(0,0,0));
		_glo->glshader->program->setUniformValue("material.shininess", 0.9f);
		_glo->_vao->bind();
		glDrawArrays(_glo->type, 0, _glo->points.size());
		_glo->_vao->release();
		//_glo->glshader->program->release();
	}


	inline static void test()
	{
		auto cam = Transform();
		std::cout << cam << std::endl;
	}
};


typedef QVector<Transform *> TransformList;