#pragma once

#include "classes.h"
#include "util.h"
#include "glview/glshader.h"


class GLObject
{
public:
	// Defaults
	Util::JRandom rnd_gen;
	// Types
	const static Int T_PTS = GL_POINTS;
	const static Int T_LINES = GL_LINES;
	const static Int T_TRIS = GL_TRIANGLES;
	const static Int T_POLYS = GL_POLYGON;
	// OpenGL
	QObject *parent;		// QOpenGLWidget pointer for GL functions
	GLShader *glshader;    // glshader pointer
	// Object variables:
	Int type;
	PointList points;      // main geometry storage
	PointList normals;
	PointList tangents;
	PointList bitangents;
	PointList colors;
	PointList uvs;
	// Qt OpenGL Stuff
	QOpenGLFunctions *f;
	QOpenGLVertexArrayObject *_vao;
	QOpenGLBuffer *_p_vbo;    // vertex positions buffer
	QOpenGLBuffer *_n_vbo;    // normals buffer
	QOpenGLBuffer *_tn_vbo;    // tangent buffer
	QOpenGLBuffer *_bt_vbo;    // bitangent buffer
	QOpenGLBuffer *_c_vbo;    // colors buffer
	QOpenGLBuffer *_uv_vbo;    // uv buffer
	// Parent properties

	GLObject(QObject *p) :
			parent(nullptr),
			glshader(nullptr),
			f(nullptr),
			type(T_PTS),
			points(PointList()),
			normals(PointList()),
			tangents(PointList()),
			bitangents(PointList()),
			colors(PointList()),
			uvs(PointList()),
			_vao(nullptr),
			_p_vbo(nullptr),
			_n_vbo(nullptr),
			_tn_vbo(nullptr),
			_bt_vbo(nullptr),
			_c_vbo(nullptr),
			_uv_vbo(nullptr)
	{
		Clear();
		parent = p;
		auto qglw_ptr = dynamic_cast<QOpenGLWidget *>(parent);
		f = qglw_ptr->context()->functions();
	}

	~GLObject() {
		Clear();
	}

	inline void Clear() {
		// Destroy OpenGL resources, test for crashes
		if (_p_vbo != nullptr) {
			_p_vbo->destroy();
			_p_vbo = nullptr;
			points.clear();
		}
		if (_n_vbo != nullptr) {
			_n_vbo->destroy();
			_n_vbo = nullptr;
			normals.clear();
		}
		if (_tn_vbo != nullptr) {
			_tn_vbo->destroy();
			_tn_vbo = nullptr;
			tangents.clear();
		}
		if (_bt_vbo != nullptr) {
			_bt_vbo->destroy();
			_bt_vbo = nullptr;
			bitangents.clear();
		}
		if (_c_vbo != nullptr) {
			_c_vbo->destroy();
			_c_vbo = nullptr;
			colors.clear();
		}
		if (_uv_vbo != nullptr) {
			_uv_vbo->destroy();
			_uv_vbo = nullptr;
			uvs.clear();
		}
		if (_vao != nullptr) {
			_vao->destroy();
			_vao = nullptr;
		}
		type = T_PTS;
		// CRASH:
		// delete glshader;
		glshader = nullptr;
		parent = nullptr;
		f = nullptr;
	}

	GLObject &operator=(const GLObject &b) {
		if (this != &b) {
			Clear();
			_p_vbo = b._p_vbo;
			_c_vbo = b._c_vbo;
			_n_vbo = b._n_vbo;
			_uv_vbo = b._uv_vbo;
			_vao = b._vao;
			//
			type = b.type;
			glshader = b.glshader;
			points = b.points;
			normals = b.normals;
			tangents = b.tangents;
			bitangents = b.bitangents;
			colors = b.colors;
			uvs = b.uvs;
			parent = b.parent;
			f = b.f;
		}
		return (*this);
	}

	[[nodiscard]] Int size() const {
		return points.size();
	}

	[[nodiscard]] inline std::string Info() const {
		std::stringstream ss;
		ss.precision(4);
		ss << fmt::format("GLObject(pts: '{}', '{}')'\n", size(), glshader->name);
		return ss.str();
	}

	friend std::ostream &operator<<(std::ostream &ostr, const GLObject &b) {
		ostr << b.Info();
		return ostr;
	}

	inline void sync_lists()
	{
		// Make sure all lists are of the same length
		typedef QVector<QVector3D> V3LIST;
		auto sz = points.size();
		if(normals.size() != points.size()) {
			normals = V3LIST(sz, QVector3D());
		}
		if(tangents.size() != points.size()) {
			tangents = V3LIST(sz, QVector3D());
		}
		if(bitangents.size() != points.size()) {
			bitangents = V3LIST(sz, QVector3D());
		}
		if(colors.size() != points.size()) {
			colors = V3LIST(sz, DEFAULT_COLOR);
		}
		if(uvs.size() != points.size()) {
			uvs = V3LIST(sz, QVector3D());
		}
	}

	[[nodiscard]] inline QOpenGLBuffer* get_new_vbo(const std::string &attr, const PointList &pts) const
	{
		if(glshader == nullptr)
			return nullptr;
		// !!! UNSAFE !!! Generates new QVector3D VBO with data
		auto vbo = new QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
		vbo->create();
		vbo->bind();
		vbo->setUsagePattern(QOpenGLBuffer::StaticDraw);
		vbo->allocate(pts.constData(), pts.size() * sizeof(QVector3D));
		auto pos = glshader->program->attributeLocation(attr.c_str());
		glshader->program->enableAttributeArray(pos);
		glshader->program->setAttributeBuffer(pos, GL_FLOAT, 0, 3, 0);
		vbo->release();
		return vbo;
	}

	// Append functions: add points/normals/colors/lines/triangles
	// another GLObject
	inline void append(const GLObject &b)
	{
		points += b.points;
		// TODO update vbos?
	}
	// QVector
	inline void append(const PointList &b)
	{
		points += b;
		// TODO update vbos?
	}
	inline void append(const QVector3D &pp)
	{
		points.append(pp);
	}
	inline void append(const QVector3D &p0, const QVector3D &p1, const QVector3D &c0=DEFAULT_COLOR, const QVector3D &c1=DEFAULT_COLOR)
	{
		points.append(p0);
		colors.append(c0);
		points.append(p1);
		colors.append(c1);
	}
	inline void append(const QVector3D &p0, const QVector3D &p1, const QVector3D &p2)
	{
		points.append(p0);
		points.append(p1);
		points.append(p2);
	}
	inline void appendNormals(const PointList &b) { normals += b; }
	inline void appendColors(const PointList &b) { colors += b; }
	inline void appendUVs(const PointList &b) { uvs += b; }

	inline void update(QObject* p)
	{
		// Upload internal data to GPU
		// TODO we're assuming here that shader was created in the same context, if not - we're presumably fucked
		if(glshader == nullptr) {return;}
		sync_lists();
		// Do we need to cleanup here?
		_vao = new QOpenGLVertexArrayObject(p);
		_vao->create();
		_vao->bind();
		glshader->program->bind();
		// vertex position VBO
		_p_vbo = get_new_vbo("position", points);
		_n_vbo = get_new_vbo("normal", normals);
		_tn_vbo = get_new_vbo("tangent", tangents);
		_bt_vbo = get_new_vbo("bitangent", bitangents);
		_c_vbo = get_new_vbo("color", colors);
		_uv_vbo = get_new_vbo("texcoords", uvs);
		// unbind everything
		glshader->program->release();
		_vao->release();
	}

	// From OpenMesh
	inline static GLObject* from_mesh(QObject* p, PolyMesh* m, const Int _type = T_TRIS, GLShader* glshader = nullptr)
	{
		// Cannot create shader without a context
		if(p == nullptr) { return nullptr; }
		// Cannot convert empty object
		if(m == nullptr) { return nullptr; }
		// Context, PolyMesh, Shadername, Allshaders, Type
		auto glo = new GLObject(p);
		glo->type = _type;
		glo->glshader = glshader;

		// 3 draw types
		PointList pts;
		PointList normals;
		auto hasVNormals = m->has_vertex_normals();
		auto hasFNormals = m->has_face_normals();
		if (_type == T_PTS) {
			for (PolyMesh::VertexIter v_it = m->vertices_begin(); v_it != m->vertices_end(); ++v_it) {
				auto h0 = m->point(*v_it);
				auto p0 = QVector3D(h0[0], h0[1], h0[2]);
				pts.append(QVector3D(p0[0], p0[1], p0[2]));
				if(hasVNormals) {
					auto n0 = m->normal(*v_it);
					normals.append(QVector3D(n0[0], n0[1], n0[2]));
				}
			}
		} else if (_type == T_LINES) {
			for (PolyMesh::VertexIter v_it = m->vertices_begin(); v_it != m->vertices_end(); ++v_it) {
			//for (PolyMesh::EdgeIter h_it = m->edges_begin(); h_it != m->edges_end(); ++h_it) {
				auto h0 = m->point(*v_it);
				auto p0 = QVector3D(h0[0], h0[1], h0[2]);
				++v_it;
				if(v_it == m->vertices_end()) { continue; }
				pts.append(QVector3D(p0[0], p0[1], p0[2]));
				auto h1 = m->point(*v_it);
				auto p1 = QVector3D(h1[0], h1[1], h1[2]);
				pts.append(QVector3D(p1[0], p1[1], p1[2]));
			}
		} else if (_type == T_TRIS) {
			if(!m->has_face_normals()) {
				m->request_face_normals();
				m->update_face_normals();
			}
			for (PolyMesh::FaceIter f_it = m->faces_begin(); f_it != m->faces_end(); ++f_it)
			{
				auto fh = m->face_handle(f_it->idx());
				auto f_norm = m->normal(fh);
				for (auto fv_it = m->fv_begin(fh); fv_it != m->fv_end(fh); ++fv_it) {
					auto p0 = m->point(*fv_it);
					pts.append(QVector3D(p0[0], p0[1], p0[2]));
					normals.append(QVector3D(f_norm[0], f_norm[1], f_norm[2]));
				}
			}
		}
		glo->append(pts);
		if(!normals.empty()) {
			glo->appendNormals(normals);
		}
		return glo;
	}
};


typedef QVector<GLObject*> GLObjectList;
