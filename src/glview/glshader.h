#pragma once
#pragma warning(disable:4305)

#include "classes.h"

// TODO: make GLShader.copy() method


class GLShader
{
public:
	std::string name;
	QObject *parent;
	QOpenGLShaderProgram *program = nullptr;
	std::string vert_path;
	std::string frag_path;
	std::string geom_path;

	/*
	explicit GLShader(QObject *p,
					  const std::string& v_path="shaders/default.vert",
					  const std::string& f_path="shaders/default.frag",
					  const std::string& g_path="shaders/default.geom")
	{
		parent = dynamic_cast<QOpenGLWidget*>(p);
		vert_path = v_path;
		frag_path = f_path;
		geom_path = g_path;
		program = load_shader(parent, vert_path, frag_path);
	}
	*/

	explicit GLShader(QObject *p, const std::string &_name = DEFAULT_SHADER, const std::string &_folder = "shaders") :
		parent(nullptr),
		program(nullptr),
		name(DEFAULT_SHADER)
	{
		parent = dynamic_cast<QOpenGLWidget *>(p);
		name = _name;
		if (name.empty())
			name = DEFAULT_SHADER;
		auto folder = Util::normpath(_folder);
		auto qname = QString::fromStdString(name);
		auto v_shd = qname + ".vert";
		auto f_shd = qname + ".frag";
		auto g_shd = qname + ".geom";

		auto pfolder = std::filesystem::path(folder);
		vert_path = (pfolder / v_shd.toStdString()).lexically_normal().string();
		frag_path = (pfolder / f_shd.toStdString()).lexically_normal().string();
		geom_path = (pfolder / g_shd.toStdString()).lexically_normal().string();
		program = load_shader(p, vert_path, frag_path, geom_path);
	}

	GLShader(const GLShader &b)
	{
		*this = b;
	}

	GLShader &operator=(const GLShader &b)
	{
		if (this != &b) {
			parent = b.parent;
			name = b.name;
			vert_path = b.vert_path;
			frag_path = b.frag_path;
			geom_path = b.geom_path;
			program = b.program;
		}
		return (*this);
	}

	[[nodiscard]] inline std::string Info() const {
		std::stringstream ss;
		ss.precision(4);
		ss << fmt::format("GLShader('{}', vert: '{}')'\n", name, vert_path);
		return ss.str();
	}

	friend std::ostream &operator<<(std::ostream &ostr, const GLShader &b) {
		ostr << b.Info();
		return ostr;
	}

	inline static QOpenGLShaderProgram *load_shader(QObject *parent, const std::string &vert_path, const std::string &frag_path, const std::string &geom_path = "")
	{
		// Create GLShader (Do not release until VAO is created)
		// https://www.qtcentre.org/threads/66481-Qt-OpenGL-(QOpenGLWidget)-Simple-Triangle
		// need to call it with auto prog = QOpenGLShaderProgram(this); prog argument
		std::cout << fmt::format("{}: Loading shader {}\n", __FUNCTION__, vert_path);
		auto v_path = Util::normpath(vert_path);
		if (v_path.empty()) {
			std::cout << fmt::format("{}: vertex shader {} does not exist\n", __FUNCTION__, v_path);
			return nullptr;
		}
		auto f_path = Util::normpath(frag_path);
		if (f_path.empty()) {
			std::cout << fmt::format("{}: fragment shader {} does not exist\n", __FUNCTION__, f_path);
			return nullptr;
		}

		auto g_path = Util::normpath(geom_path);
		if (g_path.empty()) {
			std::cout << fmt::format("{}: geometry shader {} does not exist\n", __FUNCTION__, g_path);
			//return nullptr;
		}

		auto oglw_ptr = dynamic_cast<QOpenGLWidget *>(parent);
		auto prog = new QOpenGLShaderProgram(oglw_ptr);
		auto v_res = prog->addShaderFromSourceFile(QOpenGLShader::Vertex, v_path.c_str());
		if(!v_res) { throw -1; }
		auto f_res = prog->addShaderFromSourceFile(QOpenGLShader::Fragment, f_path.c_str());
		if(!f_res) { throw -1; }
		if (!g_path.empty() && std::filesystem::exists(g_path)) {
			auto g_res = prog->addShaderFromSourceFile(QOpenGLShader::Geometry, g_path.c_str());
		}
		prog->link();
		if(!prog->isLinked()) { throw -1; }
		prog->bind();
		prog->release();
		return prog;
	}

	// Test
	inline static Int test()
	{
		return 0;
	}


};

typedef QVector<GLShader *> GLShaderList;
typedef QMap<QString, GLShader*> GLShaderDict;
