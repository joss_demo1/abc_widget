#pragma once
#pragma warning(disable:4305)

#include "classes.h"


class GLTexture
{
public:
	QObject *parent;
	QOpenGLTexture *tex = nullptr;

	explicit GLTexture(QObject *p, const std::string &filepath, const std::string &folder = "tex")
			: parent(nullptr), tex(nullptr)
	{
		Init(p, filepath, folder);
	}

	GLTexture(const GLTexture &b)
	{
		*this = b;
	}

	GLTexture &operator=(const GLTexture &b)
	{
		if (this != &b) {
			parent = b.parent;
			tex = b.tex;
		}
		return (*this);
	}

	inline void Init(const std::string &filepath, const std::string &folder = "tex")
	{
		if (p == nullptr) {
			std::cout << fmt::format("{}: Texture parent == nullptr, no OpenGL context, aborting.\n", __FUNCTION__);
			return;
		}
		parent = dynamic_cast<QOpenGLWidget *>(p);
		auto fp = filepath;
		// If empty filepath provided - use default _name
		if (fp.empty())
			fp = DEFAULT_SHADER;
		auto _path = QString::fromStdString(fp);
		auto _folder = Util::normpath(folder);
		full_path = (std::filesystem::path(_folder) / _path.toStdString()).lexically_normal().string();
		tex = load_texture(p, full_path);
	}

	inline static QOpenGLTexture *load_texture(const std::string &tex_name)
	{
		std::cout << fmt::format("{}: Loading shader {}\n", __FUNCTION__, vert_path);
		auto t_path = Util::normpath(tex_name);

		/*
		m_position_texture = new QOpenGLTexture(QOpenGLTexture::Target2D);
		m_position_texture->setSize(720, 720);
		m_position_texture->setMinificationFilter(QOpenGLTexture::Nearest);
		m_position_texture->setMagnificationFilter(QOpenGLTexture::Nearest);
		m_position_texture->setWrapMode(QOpenGLTexture::ClampToEdge);
		m_position_texture->setFormat(QOpenGLTexture::RGB16F);
		m_position_texture->allocateStorage(QOpenGLTexture::RGB, QOpenGLTexture::Float16);
		*/

		auto oglw_ptr = dynamic_cast<QOpenGLWidget *>(parent);
		auto tex = new QOpenGLTexture(oglw_ptr);
		tex->create();
		tex->setSize(m_frameWidth, m_frameHeight);
		tex->setFormat(QOpenGLTexture::R8_UNorm);
		tex->allocateStorage(QOpenGLTexture::Red, QOpenGLTexture::UInt8);
		tex->setData(QOpenGLTexture::Red, QOpenGLTexture::UInt8, srcR);
		// Set filtering modes for texture minification and  magnification
		tex->setMinificationFilter(QOpenGLTexture::Nearest);
		tex->setMagnificationFilter(QOpenGLTexture::Linear);
		tex->setWrapMode(QOpenGLTexture::ClampToBorder);
		return tex;
	}

	// Test
	inline static Int test()
	{
		return 0;
	}


};

typedef QVector<GLTexture *> GLTextureList;
typedef QMap<QString, GLTexture *> GLTextureDict;