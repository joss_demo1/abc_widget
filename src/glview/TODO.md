1. Rework GLShader/Texture/Object into generic base class with RAI (resource acquisition at initialization) and derived classes with specific overrides.
2. Convert meshes (and maybe other objects) into a plug-in system to allow to use different mesh libraries or quickly add new types.
3. Preferences window
