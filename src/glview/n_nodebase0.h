#pragma once
#pragma warning(disable:4305)

#include "classes.h"

// Base class for Scene objects.
class NodeBase
{
private:
	Util::JRandom rnd_gen;

public:
	std::string _name;    // object _name
	QVector3D _color;    // object _color

	NodeBase() : _name(DEFAULT_NAME), _color(DEFAULT_COLOR)
	{

	}

	inline virtual std::string Category()
	{
		return "base";
	}

	NodeBase &operator=(const NodeBase &b)
	{
		if (this != &b) {
			_name = b._name;
			_color = b._color;
		}
		return (*this);
	}


	inline virtual Bytes *Serialize()
	{
		// return Bytes object, binary data.
		// Maybe need to implement stream IO as well
		return new Bytes();
	}

	inline static NodeBase *Deserialize(Bytes *dump)
	{
		// Return pointer to a new NodeBase
		return new NodeBase();
	}

	inline void SetColor(RealF r = 0.0, RealF g = 0.0, RealF b = 0.0)
	{
		_color = QVector3D(r, g, b);
	}

	inline void SetColor(const QVector3D &c)
	{
		_color = c;
	}

	inline void RandomizeColor()
	{
		_color = rnd_gen.getVector();
	}
};

typedef QVector<NodeBase *> NodeBaseList;