#pragma once
#pragma warning(disable:4305)

#include "classes.h"
#include "glview/n_transform1.h"


class Light : public Transform
{
public:
	// Matrix from Transform
	// Color from NodeBase
	RealF _exposure;

	Light(const QVector3D &_pos = QVector3D(0.0, 0.0, 0.0), const QVector3D &_color = QVector3D(1.0, 1.0, 1.0), const RealF &_exposure = 1.0)
			: _exposure(1.0)
	{
		_matrix.Clear();
		_matrix.SetTranslation(_pos);
		SetColor(_color);
	}

	inline std::string Category() override {
		return CATEGORY_LIGHT;
	}

	friend std::ostream &operator<<(std::ostream &ostr, const Light &b)
	{
		// OSTR_SET
		auto t = b._matrix.Translation();
		std::stringstream ss;
		ss.precision(4);
		std::stringstream smat;
		smat.precision(4);
		smat << b._matrix;
		ss << fmt::format(
				"\nLight( _matrix({}), Color({},{},{}), Exposure({}) )",
				smat.str(),
				b._color.x(),
				b._color.y(),
				b._color.z(),
				b._exposure);
		ostr << ss.str();
		return ostr;
	}

	Light &operator=(const Light &b)
	{
		if (this != &b) {
			_exposure = b._exposure;
		}
		return (*this);
	}

	inline static void test()
	{
		auto lgt = Light();
		std::cout << lgt << std::endl;
	}
};


typedef QVector<Light *> LightList;