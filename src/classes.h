#pragma once
#pragma warning(disable:4996)	// unsafe strncpy()

#ifdef WIN32
	#define NOMINMAX
    #include <windows.h>
#endif

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cfloat>
#include <iostream>
#include <sstream>
#include <filesystem>
#include <cmath>
#include <random>
#include <set>
#include <queue>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <ctime>
#include <thread>
#include <mutex>
#include <vector>
#include <omp.h>
#include <fstream>
#include <streambuf>

// USD
/*
#include <pxr/usd/usd/stage.h>
#include <pxr/usd/usd/prim.h>
#include <pxr/usd/sdf/tokens.h>
#include <pxr/base/plug/registry.h>
#include <pxr/usd/usdUtils/introspection.h>
#include <pxr/usd/sdf/fileFormat.h>
#include <pxr/usd/usd/usdFileFormat.h>
#include <pxr/usd/ar/defaultResolver.h>
#include <pxr/usd/ar/defaultResolverContext.h>
#include <pxr/usd/ar/defineResolver.h>
#include <pxr/usd/ar/filesystemAsset.h>
#include <pxr/base/arch/fileSystem.h>
*/

// For OpenMesh:
#define _USE_MATH_DEFINES

// Alembic goes first or a lot of macro overrides happen
#include <Alembic/AbcCoreFactory/IFactory.h>
#include <Alembic/AbcGeom/IXform.h>
#include <Alembic/AbcGeom/IPolyMesh.h>

// Qt
#define QT_NO_KEYWORDS
#include <Qt>
#include <QtCore/QDebug>
#include <QtCore/QEvent>
#include <QtCore/QElapsedTimer>
#include <QtCore/QVector>
// QtGui
#include <QtGui/QCursor>
#include <QtGui/QKeyEvent>
#include <QtGui/QMouseEvent>
#include <QtGui/QVector3D>
#include <QtGui/QQuaternion>
#include <QtGui/QGenericMatrix>
#include <QtGui/QMatrix4x4>
#include <QtGui/QOpenGLFunctions>
// get all widgets at once
#include <QtWidgets/QtWidgets>
#include <QtOpenGLWidgets/qopenglwidget.h>
// Concurrency and others
#include <QtConcurrent/QtConcurrent>
#include <QSet>
// Qt OpenGL
#include <QtOpenGL/qopenglfunctions_4_5_core.h>
#include <QtOpenGL/QtOpenGL>
//#include <Qt3DCore>

// fmt
#include "fmt/format.h"
// Eigen3
//#define EIGEN_QT_SUPPORT
//#define USE_EIGEN 1	// for mat4.h
//#include "Eigen/Dense"
//#include "Eigen/Geometry"
// Openmesh
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Utils/PropertyManager.hh>	// for calc_centroid
#include <OpenMesh/Tools/Smoother/SmootherT.hh>
#include <OpenMesh/Tools/Smoother/JacobiLaplaceSmootherT.hh>
#include <OpenMesh/Tools/Subdivider/Uniform/CatmullClarkT.hh>

// LibNoise
// #include "noise/noise.h"


#include "defs.h"
