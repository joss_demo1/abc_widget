#include "classes.h"
#include "util.h"
#include "glview/input.h"
#include "glview/mat4.h"
#include "glview/n_camera2.h"
#include "glview/n_light2.h"
#include "glview/scene.h"
#include "glview/glview.h"
#include "mainwin/mainwin.h"


#ifdef __cplusplus
extern "C" {
#endif
__declspec(dllexport) DWORD NvOptimusEnablement = 1;
__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
#ifdef __cplusplus
}
#endif



int t_qt(int argc, char *argv[])
{
	QApplication a(argc, argv);
	QMainWindow w;
	w.show();
	return a.exec();
}


/*
int t_noise()
{
	using namespace noise;
	module::Perlin myModule;
	double value = myModule.GetValue (1.25, 0.75, 0.50);
	std::cout << value << std::endl;
	return 0;
}
*/

int t_avg(Int num=100)
{
	auto avg = Util::Average(num);
	for(Int i = 0; i < num; i++) avg.add(i);
	std::cout << avg.get() << std::endl;
	return 0;
}

int t_mat4()
{
	Mat4::test();
	return 0;
}

int t_cam()
{
	Camera::test();
	return 0;
}

int t_main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWin w;
    w.show();
    return a.exec();
}

int t_glview(int argc, char *argv[])
{
	return GLView::test();
}

int t_scene()
{
	return Scene::test();
}


int main(int argc, char *argv[])
{
	//t_cam();
	//t_noise();
	//t_avg();
	//t_cam();
	//t_mat4();
	//t_main(argc, argv);
	t_glview(argc, argv);
	//t_scene();
}
