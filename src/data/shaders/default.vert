#version 450 core

// --========== Uniforms
// 1. Camera
uniform mat4 mview;
uniform mat4 mproj;

// 2. Light
struct Light {
    mat4 matrix;
    vec3 color;
    float exposure;
};
const int MAX_LIGHTS = 8;
uniform int num_lights;
uniform Light lights[MAX_LIGHTS];	// array of maximum size, using just num_lights

// 3. Material
uniform Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	vec3 emission;
	float shininess;
} material;

// Object
uniform mat4 mmodel;
uniform mat3 mnormal;
//mat4 normalMatrix = transpose(inverse(modelView));


// --========== Varying
layout(location=0) in vec3 position;
layout(location=1) in vec3 normal;
layout(location=2) in vec3 tangent;
layout(location=3) in vec3 bitangent;
layout(location=4) in vec3 color;
layout(location=5) in vec3 texcoords;

// Output
layout(location=0) out Vertex
{
	vec3 position;
	vec2 texcoords;
	mat3 tangentBasis;
	vec3 vcolor;
	vec3 vnormal;
} vout;


void main()
{
	mat4 mvp = mproj * mview * mmodel;
	gl_Position = mvp * vec4(position, 1.0);
	vout.position = vec3(mmodel * vec4(position, 1.0));
	vout.texcoords = vec2(texcoords.x, 1.0-texcoords.y);
	// Pass tangent space basis vectors (for normal mapping).
	vout.tangentBasis = mat3(mvp) * mat3(tangent, bitangent, normal);
	vout.vcolor = material.diffuse;
	// Check that 1.0 here ->
	vout.vnormal = vec3(mmodel * vec4(normal, 0.0));
}
