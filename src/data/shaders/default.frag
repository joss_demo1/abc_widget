#version 450 core

// --========== Uniforms
// 1. Camera
uniform mat4 mview;
uniform mat4 mproj;

// 2. Light
struct Light {
	mat4 matrix;
	vec3 color;
	float exposure;
};
const int MAX_LIGHTS = 8;
uniform int num_lights;
uniform Light lights[MAX_LIGHTS];	// array of maximum size, using just num_lights

// 3. Material
struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	vec3 emission;
	float shininess;
};
uniform Material material;

// Object
uniform mat4 mmodel;
uniform mat3 mnormal;
//mat4 normalMatrix = transpose(inverse(modelView));


// --========== Varying
layout(location=0) in Vertex
{
	vec3 position;
	vec2 texcoords;
	mat3 tangentBasis;
	vec3 vcolor;
	vec3 vnormal;
} vin;

// Output
layout(location=0) out vec4 fColor;


vec3 blinnPhongBRDF(vec3 lightDir, vec3 viewDir, vec3 normal, vec3 diffuse, vec3 specular, float shininess) {
	vec3 color = diffuse;
	vec3 halfDir = normalize(viewDir + lightDir);
	float specDot = max(dot(halfDir, normal), 0.0);
	color += pow(specDot, shininess) * specular;
	return color;
}


vec3 get_lighting() {
	vec3 norm = normalize(vin.vnormal);
	vec3 luminance = material.ambient;

	for (int i = 0; i < num_lights; ++i) {
		vec3 l_f_vec = vec3(lights[i].matrix[3]) - vin.position;
		float att = 1.0 / length(l_f_vec);
		vec3 ldir = normalize(l_f_vec);
		vec3 vdir = normalize(vec3(mview[3]) - vin.position);
		float illuminance = dot(ldir, norm);
		if(illuminance > 0.0) {
			vec3 brdf = blinnPhongBRDF(ldir, vdir, norm, material.diffuse, material.specular, material.shininess);
			luminance += brdf * illuminance * (lights[i].color * lights[i].exposure) * att;
		}
	}
	return luminance;
}


void main() {
	vec3 lighting = get_lighting();
	fColor = vec4(lighting, 1.0);
}
